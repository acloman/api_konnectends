import dotenv from "dotenv/config";
import express from "express";
import cors from "cors";

import pkg from 'body-parser';
import db from "./app/models/index.js";
import { AuthRoute } from "./app/routes/auth.routes.js";
import { ApiRoute } from "./app/routes/api.routes.js";
import { QueryTypes } from "sequelize";
import bodyParser from "body-parser";
const { json, urlencoded } = pkg;

const app = express();

var corsOptions = {
  origin: process.env.CORS_URL,
};

var jsonParser = bodyParser.json({ limit:1024*1024*10, type:'application/json' }); 
var urlencodedParser = bodyParser.urlencoded({ extended:true, limit:1024*1024*10, type:'application/x-www-form-urlencoded' });

app.use(jsonParser);

app.use(urlencodedParser);

app.use(cors(corsOptions));

// parse requests of content-type - application/json
//app.use(json());

// parse requests of content-type - application/x-www-form-urlencoded
//app.use(urlencoded({ extended: true }));

const Role = db.role;

// Atenção, Apenas para construção de novos servers, Isso destruirá todos os dados, faça backup antes
// db.sequelize.sync({force: true}).then(() => {
//   console.log('Drop and Resync Db');
//   initial();
// });

db.sequelize.sync();

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Konnectends API is running ok!" });
});

// routes
ApiRoute(app);
AuthRoute(app);

// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

async function initial() {
  Role.create({
    name: "user"
  });
 
  Role.create({
    name: "operator"
  });
 
  Role.create({
    name: "guest"
  });

  Role.create({
    name: "surveyer"
  });
}

export const showError = (err) => {
  console.log('Erro ao realizar a query, ', err.message);
}

export const findAllCompaniesFromKMS = async () => {
  try {
    return db.sequelize.query(
      'SELECT kms.ID, kms.nome as name FROM base.clientes as kms '+   
      'LEFT JOIN konnectends.k_companies as kne '+
      'ON kms.ID = kne.ID WHERE kne.ID IS NULL;', 
      { type: QueryTypes.SELECT }
    );
  } catch (err) {
    showError(err);
    return false;
  }
}
