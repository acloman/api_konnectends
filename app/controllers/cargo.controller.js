/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAllDinamically, findElementByID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.cargo;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findCargoByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllCargos = (req, res) => (
  findAllDinamically(req, res, Instance)
);

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveCargo = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      process_id: request.process_id,
      nomeation_id: request.nomeation_id,
      name: request.name,
      description: request.description,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateCargo = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeCargo = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeCargoStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};