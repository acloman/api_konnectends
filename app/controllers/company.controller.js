/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { findAllCompaniesFromKMS } from "../../server";
import {v4 as uuidv4} from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAll, findElementByID, handleResponse, listCbx, updateData } from "./helper.controller";

// Instância de Instance DB
const Instance = db.company;

/**
 * Busca uma empresa by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findCompany = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Busca todas as empresas (paginado)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllCompanies = async (req, res) => {
  await syncNewCompaniesOnSelect();
  return findAll(req, res, Instance);
};

export const getCompaniesCbx = (req, res) => {
  return listCbx(req, res, Instance);
}

/**
 * Cria uma nova empresa
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveCompany = async (req, res) => {
  const request = req.body;

  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      name: request.name,
      fantasy_name: request.fantasy_name,
      color_system_font: request.color_system_font,
      color_system_menu: request.color_system_menu,
      color_system_submenu: request.color_system_submenu,
      logo: request.logo,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateCompany = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeCompany = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeCompanyStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};

export const syncNewCompaniesOnSelect = async () => {
  const companies = await findAllCompaniesFromKMS();
  if (!companies) return;

  try {
    return companies.map(async cp => {
      await Instance.create({
        id: cp.id,
        name: cp.name,
        fantasy_name: cp.name,
      });
    });
  } catch (error) {
    console.log('Erro ao tentar sincronizar novas empresas com o kms, ', err.message);
    return;
  }
}
