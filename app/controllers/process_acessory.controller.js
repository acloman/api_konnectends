/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, deleteAllDataByOSID, errorResponse, findAllDinamically, findElementByID, findElementByOSID, findElementByOSIDPaginated, findElementBySubgroupAndOSId, handleResponse, listCbx, updateData, updateCheckList } from "./helper.controller";

// Instância no BD
const Instance = db.processAcessory;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findProcessAcessoryByID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByID(req, res, dbInstance)
};

/**
 * Busca um item pelo ID de referência (os_id)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findProcessAcessoryByOSID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByOSID(req, res, dbInstance, req.body.os_id, false)
};

/**
 * Busca um item pelo código do subgrupo
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
 export const findProcessAcessoryBySubGroup = async (req, res) => {
    const dbInstance = await Instance;
    const { subgroup, os_id } = req.body.filter || req.body;
    const paginate = req.query && req.query.per_page ? true : false;
    findElementBySubgroupAndOSId(req, res, dbInstance, subgroup, os_id, paginate);
};

/**
* Retorna dados para uma combobox
* @param {*} req 
* @param {*} res 
* @returns 
*/
export const getProcessAcessoryCbx = async (req, res) => {
    const dbInstance = await Instance;
    listCbx(req, res, dbInstance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllProcessAccessories = async (req, res) => {
    const dbInstance = await Instance;
    findAllDinamically(req, res, dbInstance);
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveProcessAcessory = async (req, res) => {
    const dbInstance = await Instance;
    const request = req.body;
    try {
        const data = await dbInstance.create({
            id: uuidv4(),
            os_id: request.os_id,
            name: request.name,
            group: request.group,
            subgroup: request.subgroup,
            aditional_info: request.aditional_info,
            note: request.note,
            checked: request.checked,
        });
        handleResponse(res, data, 201);
    } catch (err) {
        errorResponse(err, res);
    }
};

export const updateProcessAcessory = async (req, res) => {
    const dbInstance = await Instance;
    const request = req.body;
    return updateData(request, res, dbInstance);
};

export const removeProcessAcessory = async (req, res) => {
    const dbInstance = await Instance;
    deleteData(req, res, dbInstance)
};

export const removeAllProcessAcessoryByOsID = async (req, res) => {
    const dbInstance = await Instance;
    deleteAllDataByOSID(req, res, dbInstance)
};

export const changeProcessAcessoryStatus = async (req, res) => {
    const dbInstance = await Instance;
    changeStatusData(req, res, dbInstance);
};

/**
 * Sincroniza o item (APP -> DB)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
 export const synCheckList = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  const checklist = JSON.parse(request.checklist);
  let count = 0;
  try {
    
    let results = checklist.map(async (pa) => {
      if (pa.checked == true) {
        count = count + 1;
        const qry = await dbInstance.findByPk(pa.id);
        qry.checked = pa.checked;
        await qry.save();
      }
    });

    await Promise.all(results);
    return res.status(200).send({ message: "Data successfully synchronized.", status: 'success', code: 200 });
  } catch (err) {
      console.log('PA_ERRROR', err);
      errorResponse(err, res);
  }
};