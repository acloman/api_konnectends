/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAllDinamically, findElementByID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.cargoAccessory;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findCargoAccessoryByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllCargoAccessories = (req, res) => (
  findAllDinamically(req, res, Instance)
);

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveCargoAccessory = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      company_id: request.company_id,
      cargo_id: request.cargo_id,
      name: request.name,
      description: request.description,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateCargoAccessory = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeCargoAccessory = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeCargoAccessoryStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};