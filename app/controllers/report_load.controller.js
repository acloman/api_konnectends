/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { base64Upload } from '../helpers/s3Client';
import db from "../models";
import { deleteData, errorResponse, findAllDinamically, findElementByID, findElementByOSID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.reportLoad

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportLoadByID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByID(req, res, dbInstance)
};

/**
 * Busca um item pelo ID de referência (os_id)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportLoadByOSID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByOSID(req, res, dbInstance, req.body.os_id, false)
};

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const getReportLoadCbx = async (req, res) => {
    const dbInstance = await Instance;
    listCbx(req, res, dbInstance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllReportLoad = async (req, res) => {
    const dbInstance = await Instance;
    findAllDinamically(req, res, dbInstance)
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveReportLoad = async (req, res) => {
    const request = req.body;
    let file = null;
    const dbInstance = await Instance;
    console.log("LOAD..., ", request.name);
    try {
        if (request.name && request.file) file = await base64Upload(request.name, request.file);
        await dbInstance.create({
            id: uuidv4(),
            group_id: request.group_id,
            subgroup_id: request.subgroup_id,
            os_id: request.os_id,
            name: request.name,
            description: request.description,
            isDamage: request.isDamage,
            file,
        });
        handleResponse(res, "Ok", 201);
    } catch (err) {
        errorResponse(err, res);
    }
};

export const updateReportLoad = async (req, res) => {
    const dbInstance = await Instance;
    const request = req.body;
    return updateData(request, res, dbInstance);
};

export const removeReportLoad = async (req, res) => {
    const dbInstance = await Instance;
    deleteData(req, res, dbInstance);
};