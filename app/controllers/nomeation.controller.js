/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { createNewOS } from '../helpers/database/os';
import { getFullYear } from '../helpers/dateUtils';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findByProcess, findBySurveyers, findElementByID, getLoggedUser, getNextID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.nomeation;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findNomeationByID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByID(req, res, dbInstance)
};

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllNomeations = async (req, res) => {
  const dbInstance = await Instance;
  findByProcess(req, res, dbInstance)
};

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
 export const findAllSurveyersNomeations = async (req, res) => {
  const dbInstance = await Instance;
  findBySurveyers(req, res, dbInstance)
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveNomeation = async (req, res) => {
  const request = req.body;
  const number = await getLastNomeation(res);
  const dbInstance = await Instance;
  const id = uuidv4();
  
  try {
    const data = await dbInstance.create({
      id,
      number,
      name: request.name,
      process_id: request.process_id,
      transport_type: request.transport_type,
      transport_identification: request.transport_identification,
      os_id: request.os_id,
      is_kms_survey: request.is_kms_survey,
      surveyer_company_name: request.surveyer_company_name,
      transport_type: request.transport_type,
      transport_identification: request.transport_identification,
      description: request.description,
      operation_type: request.operation_type,
      location_type: request.location_type,
      locale: request.locale,
      schedule_date: request.schedule_date,
      start_survey: request.start_survey,
      end_survey: request.end_survey,
      group_name: request.group_name,
      group_code: request.group_code,
      subgroup_name: request.subgroup_name,
      subgroup_code: request.subgroup_code ? request.subgroup_code.replace(/(?:\r\n|\r|\n)/g, ',') : '',
      load_type: request.load_type,
      particularities: request.particularities || null,
      status: request.status,
    });

    handleResponse(res, data, 201);

    if( request.subgroup_code ) handleSubgroupCode(data);

    const user = await getLoggedUser(req.userId);
    const osID = await createNewOS(user, data);
    data.os_id = osID;

    data.save();
  } catch (err) {
    errorResponse(err, res);
  }
};

const handleSubgroupCode = async (data) => {
  const { process_id, id, subgroup_code, load_type } = data;
  const cargo = await db.cargo;
  
  let stringArray = subgroup_code.split(/[ ,]+/);
  stringArray = stringArray.filter(e => String(e).trim());

  try {
    stringArray.map(name => {
      return cargo.create({
        id: uuidv4(),
        process_id,
        nomeation_id: id,
        name,
        description: load_type
      });
    });
  } catch (error) {
    console.error(error);
  }
};

export const updateNomeation = async (req, res) => {
  const request = req.body;
  // verifyAndUpdateCargos(request);
  const dbInstance = await Instance;
  return updateData(request, res, dbInstance);
};

const verifyAndUpdateCargos = async (data) => {
  const cargo = await db.cargo;
  //TODO...
};

export const removeNomeation = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance)
};

export const changeNomeationStatus = async (req, res) => {
  const dbInstance = await Instance;
  changeStatusData(req, res, dbInstance);
};

const getLastNomeation = async (res) => {
  const dbInstance = await Instance;
  let nextID = await getNextID(res, dbInstance);
  nextID += 1;
  const year = getFullYear();
  const code = `INSP${year}-00${nextID}`;
  return code;
};