/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { deleteData, getHoldInspection, errorResponse, findElementByID, findElementByOSID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.holdInspection

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findHoldInspectionByID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByID(req, res, dbInstance)
};

/**
 * Busca um item pelo ID de referência (os_id)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllHoldInspectionByOSID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByOSID(req, res, dbInstance, req.body.os_id, false)
};

/**
 * Busca um item pelo IDs de referência
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findHoldInspectionByKeys = async (req, res) => {
  const { os_id, type } = req.body;
  const dbInstance = await Instance;
  let data = null;
  try {
    data = await dbInstance.findAll({ where: { os_id, type } });
    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
};

/**
 * Busca os itens pelos IDs de referência
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findHoldInspectionAndPhotosByKeys = async (req, res) => {
  const { os_id, type } = req.body;
  const dbInstance = await Instance;
  try {
    const holdInspection = await dbInstance.findOne({ where: { os_id, type } });
    if (!holdInspection) return handleResponse(res, []);
    getPhotos(holdInspection.dataValues, res);
  } catch (err) {
    errorResponse(err, res);
  }
};

const getPhotos = async (hsp, res) => {
  const { id, os_id, type, inspection, port, ship_agent, dtInitial, dtFinal, note } = hsp;
  let data = new Array();
  const dbInstance = await db.photoHold;
  try {
    const holds = JSON.parse(inspection);
    const resolved = holds.map(async item => {
      const fileList = new Array();
      const photos = await dbInstance.findAll({ where: { os_id: hsp.os_id, hold: item.hold, type: hsp.type } });
      if (photos) {
        photos.map(photo => {
          fileList.push({ name: photo.name, file: photo.file, note: photo.note });
        });
        data.push({ ...item, 'fileList': fileList });
      }
    });
    await Promise.all(resolved);
    const response = { id, os_id, inspection: data, type, port, ship_agent, dtInitial, dtFinal, note };
    handleResponse(res, response);
  } catch (err) {
    errorResponse(err, res);
  }
}

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveHoldInspection = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  let form = await getHoldInspection(request.os_id, request.type, res);
  if (form !== null) {
    try {
      Object.keys(request).forEach((key) => {
        if (request[key] && request[key] !== null && request[key] !== "") {
          form[key] = request[key];
          console.log(key);
          console.log('Get <<< ', request[key]);
          console.log('Set >>> ', form[key]);
        }
      });
      return updateData(form, res, dbInstance, "Ok");
    } catch (error) {
      console.log("Erro ao salvar...", err);
      errorResponse(err, res);
    }
  } else {
    try {
      await dbInstance.create({
        id: uuidv4(),
        os_id: request.os_id,
        type: request.type,
        inspection: request.inspection,
        port: request.port,
        ship_agent: request.ship_agent,
        dtInitial: request.dtInitial,
        dtFinal: request.dtFinal,
        note: request.note,
      });
      handleResponse(res, "Ok", 201);
    } catch (err) {
      console.log("Erro ao salvar...", err);
      errorResponse(err, res);
    }
  }
};

export const updateHoldInspection = async (req, res) => {
  const dbInstance = await Instance;
  const request = req.body;
  return updateData(request, res, dbInstance);
};

export const removeHoldInspection = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance);
};