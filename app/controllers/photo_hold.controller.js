/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { base64Upload } from '../helpers/s3Client';
import db from "../models";
import { deleteData, errorResponse, findElementByID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.photoHold

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findPhotoHoldByID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByID(req, res, dbInstance)
};

/**
 * Busca um item pelo IDs de referência
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findPhotoHoldByKeys = async (req, res) => {
  const { os_id, hold, type } = req.body;
  const dbInstance = await Instance;
  let data = null;
  try {
    data = await dbInstance.findAll({ where: { os_id, hold, type } });
    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const savePhotoHold = async (req, res) => {
  const request = req.body;
  let file = null;
  const dbInstance = await Instance;
  try {
    if (request.name && request.file) file = await base64Upload(request.name, request.file);
    await dbInstance.create({
      id: uuidv4(),
      os_id: request.os_id,
      hold: request.hold,
      type: request.type,
      name: request.name,
      file,
      note: request.note,
    });
    handleResponse(res, "Ok", 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updatePhotoHold = async (req, res) => {
  const dbInstance = await Instance;
  const request = req.body;
  return updateData(request, res, dbInstance);
};

export const removePhotoHold = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance);
};