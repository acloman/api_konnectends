/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { Op, QueryTypes } from "sequelize";
import { showError } from "../../server";
import { formidable } from "formidable";
import s3Client from "../helpers/s3Client";
import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { getActiveOSListBySurveyer, getNomeationsSurveyers } from "../helpers/database/os";

export const nameExists = (name) => {
  if (!name) return res.status(400).send({
    message: "Failed! No name on request!",
    code: 400,
    status: 'error'
  });
};

export const getLoggedUser = async userId => await db.user.findByPk(userId);

export const getSuccessResponse = (res, response) => {
  return res.status(200).send({ response, status: 'success', code: 200 });
};

export const errorResponse = (err, res, errStatus = 500) => (
  res.status(errStatus).send({ message: err.message || "An error was occurred.", err })
);

export const handleResponse = (res, data, successCode = 200) => {
  if (data)
    return res.status(successCode).send({ data, status: 'success', code: successCode });
  else
    return res.status(404).send({ message: "Data Not found.", status: 'error', code: 404 });
};

export const handleResponseRows = (res, data, successCode = 200) => {
  if (data)
    return res.status(successCode).send({ count: data.length, rows: data, status: 'success', code: successCode });
  else
    return res.status(404).send({ message: "Data Not found.", status: 'error', code: 404 });
};

export const getPagination = (page, per_page) => {
  const limit = per_page ? + per_page : 3;
  const offset = page * limit;

  return { limit, offset };
};

export const getPagingData = (data, page, limit) => {
  const { count: totalItems, rows } = data;
  const currentPage = Number(page) + 1;
  const totalPages = Math.ceil(totalItems / limit);

  return { totalItems, rows, totalPages, currentPage, status: 'success', code: 200 };
};

export const findElementByID = async (req, res, instance) => {
  const id = req.params.id;
  try {
    const data = await instance.findByPk(id);

    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const findElementByReferenceID = async (req, res, instance, findOne = true) => {
  const reference_id = req.params.reference_id;
  let data = null;
  try {
    if (findOne)
      data = await instance.findOne({ where: { reference_id } });
    else
      data = await instance.findAll({ where: { reference_id } });

    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const findElementByOSID = async (req, res, instance, os_id, findOne = true) => {
  let data = null;
  try {
    if (findOne)
      data = await instance.findOne({ where: { os_id } });
    else
      data = await instance.findAll({ where: { os_id } });

    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const findElementByOSIDPaginated = async (req, res, instance, os_id) => {
  const { page, per_page, sort, direction } = req.query;
  const { limit, offset } = getPagination(page, per_page);
  let data = null;
  try {
    data = await instance.findAndCountAll({ where: { os_id } });
    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const findElementBySubgroupAndOSId = async (req, res, instance, subgroup, os_id, paginate = false) => {
  let data = null;
  try {
    if (paginate) {
      const { page, per_page, sort, direction } = req.query;
      const { limit, offset } = getPagination(page, per_page);
      data = await instance.findAndCountAll({ where: { os_id, subgroup } });
      const response = getPagingData(data, page, limit);
      res.send(response);
    } else {
      data = await instance.findAll({ where: { os_id, subgroup } });
      handleResponse(res, data);
    }
  } catch (err) {
    errorResponse(err, res);
  }
};

export const findElementByOSIDWithOutResponse = async (req, res, instance, os_id, findOne = true) => {
  let data = null;
  try {
    if (findOne)
      data = await instance.findOne({ where: { os_id } });
    else
      data = await instance.findAll({ where: { os_id } });

    return data;
  } catch (err) {
    errorResponse(err, res);
  }
};


export const isNameAmbiguous = async (req, res, next, instance) => {
  const name = req.body.name;
  nameExists(name);
  try {
    const data = await instance.findOne({ where: { name } });
    if (data) return res.status(400).send({ message: "Failed! name is already in use!", status: 'error', code: 400 });
    next();
  } catch (err) {
    errorResponse(err, res);
  }
};

const getQueryAttributes = (filter) => {
  let _attributes;
  let qry_params = [];
  delete filter.is_kms_admin;
  delete filter.is_active;
  Object.keys(filter).map((key) => {
    const value = filter[key].toString();
    let param;
    if (value == 'true' || value == 'false') {
      param = { [key]: { [Op.eq]: filter[key] } };
    } else {
      param = { [key]: { [Op.iLike]: `%${value}%` } };
    }
    qry_params.push(param);
    _attributes = { [Op.or]: qry_params };
  });
  return _attributes;
};

export const findAll = async (req, res, instance) => {
  const { page, per_page, sort, direction } = req.query;
  const { limit, offset } = getPagination(page, per_page);
  const { filter } = req.body;
  delete filter.is_kms_admin;
  delete filter.is_active;
  let data = {};

  try {
    if (Object.keys(filter).length === 0)
      data = await instance.findAndCountAll({ order: [[sort, direction]], limit, offset });
    else {
      const where = getQueryAttributes(filter);
      data = await instance.findAndCountAll({ where, order: [[sort, direction]], limit, offset });
    }

    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    errorResponse(err, res);
  }
};

const getQueryAttributesForCompanyID = (filter, company_id) => {
  let _attributes;
  let qry_params = [];
  Object.keys(filter).map((key) => {
    const value = filter[key].toString();
    let param;
    if (value == 'true' || value == 'false') {
      param = { [key]: { [Op.eq]: filter[key] } };
    } else {
      param = { [key]: { [Op.iLike]: `%${value}%` } };
    }
    qry_params.push(param);
    _attributes = { [Op.or]: qry_params, [Op.and]: { company_id } };
  });
  return _attributes;
};

export const findAllByCompanyID = async (req, res, instance, company_id) => {
  const { page, per_page, sort, direction } = req.query;
  const { limit, offset } = getPagination(page, per_page);
  const { filter } = req.body;
  delete filter.is_kms_admin;
  delete filter.is_active;
  let data = {};

  try {
    if (Object.keys(filter).length === 0)
      data = await instance.findAndCountAll({ where: { company_id }, order: [[sort, direction]], limit, offset });
    else {
      const where = getQueryAttributesForCompanyID(filter, company_id);
      data = await instance.findAndCountAll({ where, order: [[sort, direction]], limit, offset });
    }

    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const findAllDinamically = async (req, res, instance) => {
  const user = await getLoggedUser(req.userId);
  if (user.is_kms_admin === true)
    return findAll(req, res, instance);
  return findAllByCompanyID(req, res, instance, user.company_id);
}

export const findByProcess = async (req, res, instance) => {
  const { page, per_page, sort, direction } = req.query;
  const { limit, offset } = getPagination(page, per_page);
  const { filter } = req.body;
  delete filter.is_kms_admin;
  delete filter.is_active;
  let data = {};

  try {
    if (filter.operation_type)
      data = await instance.findAndCountAll({ where: { process_id: filter.process_id, operation_type: filter.operation_type }, order: [[sort, direction]], limit, offset });
    else
      data = await instance.findAndCountAll({ where: { process_id: filter.process_id }, order: [[sort, direction]], limit, offset });
    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const findBySurveyers = async (req, res, instance) => {
  const { page, per_page, sort, direction } = req.query;
  const { limit, offset } = getPagination(page, per_page);
  const { filter } = req.body;
  delete filter.is_kms_admin;
  delete filter.is_active;
  delete filter.clone;
  let data = {};
  try {
    const { is_kms_admin, email } = await getLoggedUser(req.userId);
    if (is_kms_admin === true)
      return findAll(req, res, instance);

    const osList = await getActiveOSListBySurveyer(email);
    if (osList) {
      let osArray = [];

      osList.map(row => {
        osArray.push(row.os_id);
      });

      const osStringList = osArray.join("', '");
      data = {count: 0, rows: []};
      data.rows = await getNomeationsSurveyers(osStringList);
      if (data.rows.length > 0) data.count = data.rows.length;
    }



    const response = getPagingData(data, page, limit);
    res.send(response);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const listCbx = async (req, res, instance, useDescription = false, showAll = false) => {
  let data = null;
  try {
    const user = await getLoggedUser(req.userId);
    if (user.is_kms_admin === true || showAll == true)
      data = await instance.findAll({
        attributes: ['id', useDescription ? 'description' : 'name']
      });
    else
      data = await instance.findAll({
        attributes: ['id', useDescription ? 'description' : 'name'],
        where: { 'company_id': user.company_id }
      });
    handleResponse(res, data);
  } catch (error) {
    errorResponse(error, res);
  }
};

export const deleteData = (req, res, instance) => {
  const id = req.params.id;
  try {
    instance.destroy({ where: { id } });
    return res.status(200).send({ message: "Data successful deleted.", status: 'success', code: 204 });
  } catch (err) {
    errorResponse(err, res);
  }
};

export const deleteAllDataByOSID = (req, res, instance) => {
  const os_id = req.params.os_id;
  try {
    instance.destroy({ where: { os_id } });
    return res.status(200).send({ message: "Data successful deleted.", status: 'success', code: 204 });
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateCheckList = async (pa, instance) => {
  try {
    const qry = await instance.findByPk(pa.id);
    Object.assign(qry, pa);

    const data = await qry.save();
    return;
  } catch (err) {
    console.log('Error on Save Checklist', err);
  }
};

export const updateData = async (req, res, instance, customResponse = null) => {
  try {
    const qry = await instance.findByPk(req.id);
    Object.assign(qry, req);

    const data = await qry.save();
    handleResponse(res, customResponse || data);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const changeStatusData = async (req, res, instance) => {
  try {
    const qry = await instance.findByPk(req.body.id);
    qry.is_active = !qry.is_active;

    const data = await qry.save();

    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const changeStatusDescription = async (req, res, instance, newStatus) => {
  try {
    const qry = await instance.findByPk(req.body.id);
    qry.status = newStatus;

    const data = await qry.save();

    handleResponse(res, data);
  } catch (err) {
    errorResponse(err, res);
  }
}

export const getNextID = async (res, instance) => {
  try {
    const qry = await instance.count({ distinct: 'id' });
    return qry;
  } catch (err) {
    errorResponse(err, res);
  }
}

export const findAllProductsFromKMS = async () => {
  try {
    return db.sequelize.query(
      'SELECT * FROM base.produtos', { type: QueryTypes.SELECT }
    );
  } catch (err) {
    showError(err);
    return false;
  }
}

export const findAllShipsFromKMS = async (req, res) => {
  try {
    const data = await db.sequelize.query(
      'SELECT nome FROM base.navios', { type: QueryTypes.SELECT }
    );
    handleResponse(res, data);
  } catch (err) {
    showError(err);
    return false;
  }
}

export const findShipIDByExactNameFromKMS = async (name) => {
  try {
    return db.sequelize.query(
      `SELECT id FROM base.navios WHERE nome = '${name}'`, { type: QueryTypes.SELECT }
    );
  } catch (err) {
    showError(err);
    return false;
  }
}

export const newShip = async (name) => {
  try {
    return db.sequelize.query(
      `INSERT INTO base.navios (id, nome) VALUES ('${uuidv4()}', '${name}')`, { type: QueryTypes.INSERT }
    );
  } catch (err) {
    showError(err);
    return false;
  }
}

export const handleFile = async (req, res, next) => {
  const form = new formidable.IncomingForm();
  try {
    form.parse(req, async (err, fields, files) => {
      await s3Client.uploadFile(files.filetoupload.name, files.filetoupload.path);
      next();
    });
  } catch (err) {
    errorResponse(err, res);
  }
}

/** REPORT CONTROLLER PAYLOADS */

export const getNomeation = async (os_id, res) => {
  const instance = await db.nomeation;
  try {
    const data = await instance.findOne({ where: { os_id } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar a nomeação para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getAccessories = async (os_id, subgroup, res) => {
  const instance = await db.processAcessory;
  try {
    const data = await instance.findAll({ where: { os_id, subgroup } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar os acessórios para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getHoldInspection = async (os_id, type, res) => {
  const instance = await db.holdInspection;
  try {
    const data = await instance.findOne({ where: { os_id, type } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar o formulário para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getFormReport = async (os_id, res) => {
  const instance = await db.reportForm;
  try {
    const data = await instance.findOne({ where: { os_id } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar o formulário para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getLoadingPitures = async (os_id, res) => {
  const instance = await db.reportLoad;
  try {
    const data = await instance.findAll({ where: { os_id } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar imagens de carregamento para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getPreLoadingPitures = async (os_id, res) => {
  const instance = await db.reportPreLoad;
  try {
    const data = await instance.findAll({ where: { os_id } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar imagens de pré-carregamento para montar o relatório: ', error);
    errorResponse(error, res);
  }
}

export const getCheckList = async (os_id, res) => {
  const instance = await db.processAcessory;
  try {
    const data = await instance.findAll({ where: { os_id } });
    return data || null;
  } catch (error) {
    console.log('Erro ao recuperar check-lists para montar o relatório: ', error);
    errorResponse(error, res);
  }
}
