/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { base64Upload } from '../helpers/s3Client';
import db from "../models";
import { deleteData, errorResponse, findAllDinamically, findElementByID, findElementByOSIDPaginated, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.reportPreLoad

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportPreloadByID = (req, res) => (
    findElementByID(req, res, Instance)
);

/**
 * Busca um item pelo ID de referência (os_id)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportPreloadByOSID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByOSIDPaginated(req, res, dbInstance, req.body.filter.os_id)
};

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const getReportPreloadCbx = async (req, res) => {
  const dbInstance = await Instance;
  listCbx(req, res, dbInstance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllReportPreload = async (req, res) => {
  const dbInstance = await Instance;
    findAllDinamically(req, res, dbInstance)
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveReportPreload = async (req, res) => {
    const request = req.body;
    let file = null;
    const dbInstance = await Instance;
    try {
        if (request.name) file = await base64Upload(request.name, request.file);
        await dbInstance.create({
            id: uuidv4(),
            group_id: request.group_id,
            subgroup_id: request.subgroup_id,
            os_id: request.os_id,
            name: request.name,
            description: request.description,
            isDamage: request.isDamage,
            file,
        });
        handleResponse(res, "Ok", 201);
    } catch (err) {
        errorResponse(err, res);
    }
};

export const updateReportPreload = async (req, res) => {
    const dbInstance = await Instance;
    const request = req.body;
    return updateData(request, res, dbInstance);
};

export const removeReportPreload = async (req, res) => {
    const dbInstance = await Instance;
    deleteData(req, res, dbInstance)
};