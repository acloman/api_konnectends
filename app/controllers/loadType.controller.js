/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { hashSync } from "bcryptjs";
import {v4 as uuidv4} from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAll, findElementByID, handleResponse, listCbx, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.loadType;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findLoadTypeByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
 export const getLoadTypesCbx = (req, res) => {
  listCbx(req, res, Instance, false, true);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllLoadTypes = (req, res) => (
  findAll(req, res, Instance)
);

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveLoadType = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      name: request.name,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateLoadType = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeLoadType = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeLoadTypeStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};