/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { vehicleCargoReport } from "../helpers/report/vehicleCargoReport";
import { errorResponse } from "./helper.controller";

export const generateReport = async (req, res) => {
  console.log('Param', req.params)
  const { os_id } = req.params;
  try {
    await vehicleCargoReport(res, os_id);
    console.log('Report generation done!');
    return res;
  } catch (error) {
    return errorResponse(`An error has occurred on generate a report file... ${error}`, res)
  }
}