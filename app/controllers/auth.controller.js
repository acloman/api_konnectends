import db from "../models";
import { secret } from "../config/auth.config";
import { sign } from "jsonwebtoken";
import { compareSync } from "bcryptjs";

const User = db.user;

export const signin = async (req, res) => {
  try {
    
    const user = await User.findOne({ where: { email: req.body.email } });
    if (!user) return res.status(404).send({ message: 'User Not found.' });

    const passwordIsValid = compareSync( req.body.password, user.password );
    if (!passwordIsValid)
      return res.status(401).send({ accessToken: null, message: 'Invalid Password!' });

    if(!user.is_active) return res.status(401).send({ 
      accessToken: null, message: 'User disabled on system' 
    });

    const token = sign({ id: user.id }, secret, {
      expiresIn: 86400 // 24 hours
    });

    return res.status(200).send({ user, token });

  } catch (err) {
    res.status(500).send({ message: err.message });
  }
}
