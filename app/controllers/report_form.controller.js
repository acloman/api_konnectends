/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { base64Upload } from '../helpers/s3Client';
import db from "../models";
import { getFormReport, deleteData, errorResponse, findAllDinamically, findElementByID, findElementByOSID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.reportForm

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportFormByID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByID(req, res, dbInstance)
};

/**
 * Busca um item pelo ID de referência (os_id)
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportFormByOSID = async (req, res) => {
    const dbInstance = await Instance;
    findElementByOSID(req, res, dbInstance, req.body.os_id)
};

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const getReportFormsCbx = async (req, res) => {
    const dbInstance = await Instance;
    listCbx(req, res, dbInstance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllReportForms = async (req, res) => {
    const dbInstance = await Instance;
    findAllDinamically(req, res, dbInstance)
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveReportForm = async (req, res) => {
    const request = req.body;
    const dbInstance = await Instance;
    let foto_proa = null;
    let foto_popa = null;

    let reportForm = await getFormReport(request.os_id, res);
    if (reportForm !== null) {
      if (request.nome_foto_proa && request.foto_proa) reportForm.foto_proa = await base64Upload(request.nome_foto_proa, request.foto_proa);
      if (request.nome_foto_popa && request.foto_popa) reportForm.foto_popa = await base64Upload(request.nome_foto_popa, request.foto_popa);
      Object.keys(request).forEach((key) => {
        if (request[key] && request[key] !== null && request[key] !== "") {
          reportForm[key] = request[key];
          console.log(key);
          console.log('Get <<< ', request[key]);
          console.log('Set >>> ', reportForm[key]);
        }
      });
      return updateData(reportForm, res, dbInstance, "Ok");
    } else {
      try {
        if (request.nome_foto_proa && request.foto_proa) foto_proa = await base64Upload(request.nome_foto_proa, request.foto_proa);
        if (request.nome_foto_popa && request.foto_popa) foto_popa = await base64Upload(request.nome_foto_popa, request.foto_popa);
        await dbInstance.create({
            id: uuidv4(),
            reference_id: request.reference_id,
            os_id: request.os_id,
            nome_navio: request.nome_navio,
            dt_chegada_navio: request.dt_chegada_navio,
            dt_atracacao: request.dt_atracacao,
            pratico_a_bordo: request.pratico_a_bordo,
            berco: request.berco,
            terminal: request.terminal,
            armador: request.armador,
            agente_navio: request.agente_navio,
            operador_portuario: request.operador_portuario,
            ultimo_porto: request.ultimo_porto,
            proximo_porto: request.proximo_porto,
            inicio_operacao: request.inicio_operacao,
            fim_operacao: request.fim_operacao,
            tempo_clima: request.tempo_clima,
            nome_foto_proa: request.nome_foto_proa,
            nome_foto_popa: request.nome_foto_popa,
            foto_proa,
            foto_popa,
        });
        handleResponse(res, "Ok", 201);
      } catch (err) {
          console.log("Erro ao salvar...", err);
          errorResponse(err, res);
      }
    }
};

export const updateReportForm = async (req, res) => {
    const dbInstance = await Instance;
    const request = req.body;
    return updateData(request, res, dbInstance);
};

export const removeReportForm = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance);
};