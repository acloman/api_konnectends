/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import { getFullYear } from '../helpers/dateUtils';
import db from "../models";
import { changeStatusData, changeStatusDescription, deleteData, errorResponse, findAllDinamically, findElementByID, getNextID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.process;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findProcessByID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByID(req, res, dbInstance)
};

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllProcess = async (req, res) => {
  const dbInstance = await Instance;
  findAllDinamically(req, res, dbInstance);
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveProcess = async (req, res) => {
  const request = req.body;
  const p_number = await getLastProcess(res);
  const dbInstance = await Instance;
  try {
    const data = dbInstance.create({
      id: uuidv4(),
      process_number: p_number,
      description: request.description,
      company_id: request.company_id,
      company_name: request.company_name,
      customer_id: request.customer_id,
      customer_name: request.customer_name,
      ship_name: request.ship_name || null,
      ship_imo: request.ship_imo || null,
      load_type: request.load_type || null,
      status: request.status,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateProcess = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  return updateData(request, res, dbInstance);
};

export const removeProcess = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance)
};

export const changeProcessStatus = async (req, res) => {
  const dbInstance = await Instance;
  changeStatusData(req, res, dbInstance);
};

export const changeProcessStatusDescription = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  changeStatusDescription(req, res, dbInstance, request.status);
}

export const getLastProcess = async (res) => {
  const dbInstance = await Instance;
  let nextID = await getNextID(res, dbInstance);
  nextID += 1;
  const year = getFullYear();
  const code = `PRC${year}-00${nextID}`;
  return code;
};
