/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { hashSync } from "bcryptjs";
import {v4 as uuidv4} from 'uuid';
import { getInspectorID, getInspectorNomeations } from "../helpers/database/os";
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAllDinamically, findElementByID, getLoggedUser, handleResponse, handleResponseRows, updateData } from "./helper.controller";

// Instância de Usuário DB
const Instance = db.user;

/**
 * Busca um usuário by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findUserByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Busca todos os usuários da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllUsers = async (req, res) => {
  return findAllDinamically(req, res, Instance);
};

/**
 * Cria um novo usuário
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveUser = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      name: request.name,
      email: request.email,
      password: hashSync(request.password, 8),
      company_id: request.company_id,
      company_name: request.company_name,
      role: request.role,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateUser = async (req, res) => {
  const request = req.body;
  if(request.password) request.password = hashSync(request.password, 8);
  return updateData(request, res, Instance);
};

export const removeUser = (req, res) => (
  deleteData(req, res, Instance)
);
 
export const changeUserStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};

export const appGetNomeations = async (req, res) => {
  const user = await getLoggedUser(req.userId);
  const { email } = user;
  const inspectorID = await getInspectorID(email);
  if (!inspectorID || inspectorID.length === 0) return errorResponse('E-mail do Usuário não cadastrado como inspetor', res);
  const data = await getInspectorNomeations(inspectorID[0].inspetor_id);
  const pa_dbInstance = await db.processAcessory;
  // Adiciona os checklists ao Payload.
  let results = data.map(async (item, idx) => {
    const pas = await pa_dbInstance.findAll({ where: { os_id: item.os_id } });
    let checklist = [];
    pas.map(pa => {
      if (pa && pa.is_active === true) {
        const chk = { id: pa.id, name: pa.name, group: pa.group, subgroup: pa.subgroup, checked: pa.checked, note: pa.note };
        checklist.push(chk);
      }  
    });
    data[idx] = {...item, 'checklist': checklist}
  });
  await Promise.all(results);
  // ...
  
  handleResponseRows(res, data, 201);
};
