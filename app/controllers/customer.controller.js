/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAllDinamically, findElementByID, handleResponse, listCbx, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.customer;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findCustomerByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
export const getCustomersCbx = (req, res) => {
  listCbx(req, res, Instance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllCustomers = (req, res) => (
  findAllDinamically(req, res, Instance)
);

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveCustomer = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      company_id: request.company_id,
      company_name: request.company_name,
      name: request.name,
      fantasy_name: request.fantasy_name,
      contact_name: request.contact_name,
      contact_email: request.contact_email,
      contact_phone: request.contact_phone,
      addresse_line_1: request.addresse_line_1,
      addresse_line_2: request.addresse_line_2,
      zip_code: request.zip_code,
      state: request.state,
      country: request.country,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateCustomer = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeCustomer = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeCustomerStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};