/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { v4 as uuidv4 } from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findElementByID, findElementByOSID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.reportFile;

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportFileByID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByID(req, res, dbInstance)
};

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findReportFileByOSID = async (req, res) => {
  const dbInstance = await Instance;
  findElementByOSID(req, res, dbInstance, req.body.os_id, false)
};

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveReportFile = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  try {
    const data = await dbInstance.create({
      id: uuidv4(),
      os_id: request.os_id,
      company_id: request.company_id,
      name: request.name,
      description: request.description,
      type: request.type,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateReportFile = async (req, res) => {
  const request = req.body;
  const dbInstance = await Instance;
  return updateData(request, res, dbInstance);
};

export const removeReportFile = async (req, res) => {
  const dbInstance = await Instance;
  deleteData(req, res, dbInstance)
};

export const changeReportFileStatus = async (req, res) => {
  const dbInstance = await Instance;
  changeStatusData(req, res, dbInstance);
};