/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import {v4 as uuidv4} from 'uuid';
import db from "../models";
import { changeStatusData, deleteData, errorResponse, findAllDinamically, findElementByID, handleResponse, updateData } from "./helper.controller";

// Instância no BD
const Instance = db.accessory

/**
 * Busca um item by ID na base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAccessoryByID = (req, res) => (
  findElementByID(req, res, Instance)
);

/**
 * Retorna dados para uma combobox
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
 export const getAcessoriesCbx = (req, res) => {
  listCbx(req, res, Instance);
}

/**
 * Busca todos os itens da base
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const findAllAccessories = (req, res) => (
  findAllDinamically(req, res, Instance)
);

/**
 * Cria um novo item
 * @param {*} req 
 * @param {*} res 
 * @returns http response
 */
export const saveAccessory = async (req, res) => {
  const request = req.body;
  try {
    const data = await Instance.create({
      id: request.id || request.id !== null ? request.id : uuidv4(),
      name: request.name,
      company_id: request.company_id,
      company_name: request.company_name,
      description: request.description,
    });
    handleResponse(res, data, 201);
  } catch (err) {
    errorResponse(err, res);
  }
};

export const updateAccessory = async (req, res) => {
  const request = req.body;
  return updateData(request, res, Instance);
};

export const removeAccessory = (req, res) => (
  deleteData(req, res, Instance)
);

export const changeAccessoryStatus = (req, res) => {
  changeStatusData(req, res, Instance);
};