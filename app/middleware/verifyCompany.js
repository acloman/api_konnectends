import { nameExists } from "../controllers/helper.controller";
import db  from "../models";

const Company = db.company;

export const checkDuplicateCompany = (req, res, next) => {
  nameExists(req.body.name);
  Company.findOne({
    where: {
      name: req.body.name
    }
  }).then(data => {
    if (data) {
      res.status(400).send({
        message: "Failed! Company name is already in use!"
      });
      return;
    }

    Company.findOne({
      where: {
        fantasy_name: req.body.fantasy_name
      }
    }).then(data => {
      if (data) {
        res.status(400).send({
          message: "Failed! Fantasy name of company is already in use!"
        });
        return;
      }

      next();
    });
  });
};