import { isNameAmbiguous } from "../controllers/helper.controller";
import db  from "../models";

export const checkDuplicateGroup = (req, res, next) => {
  const Instance = db.group;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicateSubGroup = (req, res, next) => {
  const Instance = db.subGroup;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicateLoadType = (req, res, next) => {
  const Instance = db.loadType;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicateLocationType = (req, res, next) => {
  const Instance = db.locationType;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicateOperationType = (req, res, next) => {
  const Instance = db.operationType;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicatePhotoHold = async (req, res, next) => {
  const Instance = await db.photoHold;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicatePreload = async (req, res, next) => {
  const Instance = await db.reportPreLoad;
  return isNameAmbiguous(req, res, next, Instance);
};

export const checkDuplicateLoad = async (req, res, next) => {
  const Instance = await db.reportLoad;
  return isNameAmbiguous(req, res, next, Instance);
};
