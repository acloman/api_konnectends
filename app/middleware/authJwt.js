import { verify } from "jsonwebtoken";
import { secret } from "../config/auth.config.js";
import db from "../models";
const User = db.user;

const getToken = (bearerHeader) => {
  const bearer = bearerHeader ? bearerHeader.split(' ') : null;
  return bearer !== null ? bearer[1] : null;
}

const verifyToken = (req, res, next) => {
  const token = getToken(req.headers['authorization']);
  // let token = req.headers["x-access-token"];

  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }

  verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }

    req.token = token;
    req.userId = decoded.id;
    next();
  });
};

const isAdmin = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    if (user.role === "admin" || user.is_kms_admin) {
      next();
      return;
    } else {
      res.status(403).send({
        message: "Require Admin Role!"
      });
      return;
    }
  });
};

const isSuperAdmin = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    if (user.is_kms_admin === true) {
      next();
      return;
    } else {
      res.status(403).send({
        message: "Require Super Admin Role!"
      });
      return;
    }
  });
};

const checkIsSuperAdmin = async (req) => {
  let isSuperAdmin = false;
  await User.findByPk(req.userId).then(user => {
    if (user.is_kms_admin === true) {
      isSuperAdmin = true;
    }
  });
  return isSuperAdmin
};

const isOperator = (req, res, next) => {
  User.findByPk(req.userId).then(user => {
    if (user.role === "operator") {
      next();
      return;
    } else {
      res.status(403).send({
        message: "Require Operator Role!"
      });
      return;
    }
  });
};

const isOperatorOrAdmin = (req, res, next) => {
  const isSuperAdmin = checkIsSuperAdmin(req);
  User.findByPk(req.userId).then(user => {
    if (user.role === "operator" || user.role === "admin" || isSuperAdmin) {
      next();
      return;
    } else {
      res.status(403).send({
        message: "Require Operator or Admin Role!"
      });
      return;
    }
  });
};

const isSurveyerOrSuperAdmin = (req, res, next) => {
  const isSuperAdmin = checkIsSuperAdmin(req);
  User.findByPk(req.userId).then(user => {
    if (user.role === "surveyer" || isSuperAdmin) {
      next();
      return;
    } else {
      res.status(403).send({
        message: "Require Surveyer or Super Admin Role!"
      });
      return;
    }
  });
};

const authJwt = {
  'verifyToken': verifyToken,
  'isAdmin': isAdmin,
  'isSuperAdmin': isSuperAdmin,
  'isOperator': isOperator,
  'isOperatorOrAdmin': isOperatorOrAdmin,
  'isSurveyerOrSuperAdmin': isSurveyerOrSuperAdmin
};
export default authJwt;