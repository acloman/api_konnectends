import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const Process = async (sequelize, Sequelize) => {
  return sequelize.define('k_processes', {
    id: uuidPrimaryKey(),
    process_number: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    company_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    company_name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    customer_id: {
      allowNull: true,
      type: DataTypes.UUID,
      defaultValue: null,
    },
    customer_name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    ship_name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    ship_imo: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    load_type: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: 'CREATED', // CREATED, SCHEDULED, OPERATING, DONE, CANCELED, WAITHING
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};