import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const GroupFile = (sequelize, Sequelize) => {
  return sequelize.define('k_group_files', {
    id: uuidPrimaryKey(),
    process_id: {
      allowNull: true,
      type: DataTypes.UUID,
      defaultValue: null,
    },
    company_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    group_number: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    file: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};