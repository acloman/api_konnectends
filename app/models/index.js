import { DB, USER, PASSWORD, HOST, dialect as _dialect, schemaName as _schemaName, pool as _pool } from "../config/db.config.js";
import Sequelize from 'sequelize';
import { User } from "./user.model.js";
import { Role } from "./role.model.js";
import { Company } from "./company.model.js";
import { Accessory } from "./accessory.model.js";
import { Group } from "./group.model.js";
import { SubGroup } from "./subGroup.model.js";
import { LoadType } from "./loadType.model.js";
import { LocationType } from "./locationType.model.js";
import { OperationType } from "./operationType.model.js";
import { Process } from "./process.model.js";
import { Cargo } from "./cargo.model.js";
import { Customer } from "./customer.model.js";
import { Nomeation } from "./nomeation.model.js";
import { CargoAccessory } from "./cargo_accessory.model.js";
import { ProcessAcessory } from "./process_acessory.model.js";
import { reportFile } from "./report_file.model.js";
import { GroupFile } from "./group_file.model.js";
import { ReportForm } from "./report_form.model.js";
import { ReportLoad } from "./report_load.model.js";
import { ReportPreload } from "./report_preload.model.js";
import { HoldInspection } from "./hold_inspection.model.js";
import { PhotoHold } from "./photo_hold.model.js";

const sequelize = new Sequelize(
  DB,
  USER,
  PASSWORD,
  {
    host: HOST,
    dialect: _dialect,
    operatorsAliases: false,

    pool: {
      max: _pool.max,
      min: _pool.min,
      acquire: _pool.acquire,
      idle: _pool.idle
    }
  },
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// Cria as tabelas caso não existam e instanciam no sistema.
db.user = User(sequelize, Sequelize);
db.role = Role(sequelize, Sequelize);
db.company = Company(sequelize, Sequelize);
db.customer = Customer(sequelize, Sequelize);
db.accessory = Accessory(sequelize, Sequelize);
db.group = Group(sequelize, Sequelize);
db.subGroup = SubGroup(sequelize, Sequelize);
db.loadType = LoadType(sequelize, Sequelize);
db.locationType = LocationType(sequelize, Sequelize);
db.operationType = OperationType(sequelize, Sequelize);
db.process = Process(sequelize, Sequelize);
db.cargo = Cargo(sequelize, Sequelize);
db.nomeation = Nomeation(sequelize, Sequelize);
db.cargoAccessory = CargoAccessory(sequelize, Sequelize);
db.processAcessory = ProcessAcessory(sequelize, Sequelize);
db.reportFile = reportFile(sequelize, Sequelize);
db.groupFile = GroupFile(sequelize, Sequelize);
db.reportForm = ReportForm(sequelize, Sequelize);
db.reportLoad = ReportLoad(sequelize, Sequelize);
db.reportPreLoad = ReportPreload(sequelize, Sequelize);
db.holdInspection = HoldInspection(sequelize, Sequelize);
db.photoHold = PhotoHold(sequelize, Sequelize);

db.ROLES = ["admin", "operator", "guest"];

export default db;
