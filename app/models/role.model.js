import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const Role = (sequelize, Sequelize) => {
  return sequelize.define('k_roles', {
    id: uuidPrimaryKey(),
    name: {
      type: Sequelize.STRING
    }
  }, {
    schema: _schemaName
  });
};
