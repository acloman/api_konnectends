import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const ProcessAcessory = async (sequelize, Sequelize) => {
  return sequelize.define('k_process_acessories', {
    id: uuidPrimaryKey(),
    os_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    group: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: '1',
    },
    subgroup: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    aditional_info: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    note: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    checked: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};