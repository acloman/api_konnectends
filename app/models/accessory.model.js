import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const Accessory = (sequelize, Sequelize) => {
  return sequelize.define('k_accessories', {
    id: uuidPrimaryKey(),
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    company_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    company_name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};