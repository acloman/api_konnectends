import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const PhotoHold = async (sequelize, Sequelize) => {
  return sequelize.define('k_photo_holds', {
    id: uuidPrimaryKey(),
    os_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    hold: { // 0, 1, 2, 3 ... 10 (0 = GENERAL)
      type: Sequelize.STRING,
      allowNull: false,
    },
    type: { // HOLD, HOSE, DRAFT, ETC...
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    file: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    note: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  }, {
    schema: _schemaName
  });
};