import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const reportFile = (sequelize, Sequelize) => {
  return sequelize.define('k_report_files', {
    id: uuidPrimaryKey(),
    os_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    company_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    file: {
      type: Sequelize.STRING,
      defaultValue: 'pdf'
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};