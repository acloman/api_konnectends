import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const OperationType = (sequelize, Sequelize) => {
  return sequelize.define('k_operationtypes', {
    id: uuidPrimaryKey(),
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};