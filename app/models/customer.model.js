import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const Customer = (sequelize, Sequelize) => {
  return sequelize.define('k_customers', {
    id: uuidPrimaryKey(),
    company_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    company_name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    fantasy_name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    contact_name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    contact_email: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    contact_phone: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    addresse_line_1: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    addresse_line_2: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    zip_code: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    state: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    country: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};