import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const Company = (sequelize, Sequelize) => {
  return sequelize.define('k_companies', {
    id: uuidPrimaryKey(),
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    fantasy_name: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    color_system_font: {
      type: Sequelize.STRING,
      defaultValue: '#aaa',
      allowNull: true,
    },
    color_system_menu: {
      type: Sequelize.STRING,
      defaultValue: '#000',
      allowNull: true,
    },
    color_system_submenu: {
      type: Sequelize.STRING,
      defaultValue: '#eee',
      allowNull: true,
    },
    logo: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    logo_background: {
      type: Sequelize.STRING,
      defaultValue: '#fff',
      allowNull: true,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};