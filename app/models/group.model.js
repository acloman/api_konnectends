import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const Group = (sequelize, Sequelize) => {
  return sequelize.define('k_groups', {
    id: uuidPrimaryKey(),
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};