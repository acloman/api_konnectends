import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const Cargo = async (sequelize, Sequelize) => {
  return sequelize.define('k_cargo', {
    id: uuidPrimaryKey(),
    process_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    nomeation_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
  }, {
    schema: _schemaName
  });
};