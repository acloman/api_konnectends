import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const ReportLoad = async (sequelize, Sequelize) => {
  return sequelize.define('k_report_loads', {
    id: uuidPrimaryKey(),
    group_id: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    subgroup_id: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    os_id: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    isDamage: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    file: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  }, {
    schema: _schemaName
  });
};