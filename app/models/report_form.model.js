import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

export const ReportForm = async (sequelize, Sequelize) => {
  return sequelize.define('k_report_forms', {
    id: uuidPrimaryKey(),
    reference_id: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    os_id: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    nome_navio: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    dt_chegada_navio: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    dt_atracacao: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    pratico_a_bordo: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    berco: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    terminal: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    armador: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    agente_navio: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    operador_portuario: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    ultimo_porto: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    proximo_porto: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    inicio_operacao: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    fim_operacao: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    tempo_clima: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    nome_foto_proa: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    nome_foto_popa: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    foto_proa: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    foto_popa: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  }, {
    schema: _schemaName
  });
};