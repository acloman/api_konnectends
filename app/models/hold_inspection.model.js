import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";

/**
 * Agrupador de fotos de inspeção de porões, as fotos (photo_hold.model.js), são agrupadas nesta tabela, utiliza chave composta,
 * ( os_id, hold e type ) nas duas tabelas para localizar e agrupar. 
 * @param {*} sequelize 
 * @param {*} Sequelize 
 * @returns 
 */

export const HoldInspection = async (sequelize, Sequelize) => {
  return sequelize.define('k_hold_inspections', {
    id: uuidPrimaryKey(),
    os_id: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    inspection: { // Ex.: { holds: [{ hold: 1, approved: false }, { hold: 4, approved: true }, ...]} mas DRAFT pode ser outro objeto JSON
      allowNull: false,
      type: Sequelize.JSONB,
      defaultValue: null,
    },
    type: { // HOLD, HOSE, DRAFT, ETC...
      type: Sequelize.STRING,
      allowNull: false,
    },
    port: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    ship_agent: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    },
    dtInitial: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    },
    dtFinal: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null
    },
    note: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
  }, {
    schema: _schemaName
  });
};