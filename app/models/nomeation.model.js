import { uuidPrimaryKey } from "../helpers/database/uuid";
import { schemaName as _schemaName } from "../config/db.config.js";
import { DataTypes } from "sequelize";

export const Nomeation = (sequelize, Sequelize) => {
  return sequelize.define('k_nomeation', {
    id: uuidPrimaryKey(),
    number: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    name: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    process_id: {
      allowNull: false,
      type: DataTypes.UUID,
    },
    transport_type: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    transport_identification: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: '-',
    },
    os_id: {
      allowNull: true,
      type: DataTypes.UUID,
      defaultValue: null,
    },
    is_kms_survey: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    surveyer_company_name: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: 'KMS',
    },
    description: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: true,
    },
    operation_type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    location_type: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    locale: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    schedule_date: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    start_survey: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    end_survey: {
      type: Sequelize.STRING,
      allowNull: true,
      defaultValue: null,
    },
    group_name: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    group_code: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    subgroup_name: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    subgroup_code: {
      allowNull: true,
      type: Sequelize.STRING,
      defaultValue: null,
    },
    load_type: {
      allowNull: false,
      type: Sequelize.STRING,
    },
    particularities: {
      allowNull: true,
      type: Sequelize.JSONB,
      defaultValue: null,
    },
    status: {
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: "CREATED" // IN_PROGRESS, PAUSED, CANCELED, DONE
    },
    is_active: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
  }, {
    schema: _schemaName
  });
};