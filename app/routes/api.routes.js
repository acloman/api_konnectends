import authJwt from "../middleware/authJwt";
import { findUserByID, findAllUsers, saveUser, updateUser, removeUser, changeUserStatus, appGetNomeations } from "../controllers/user.controller";
import { changeCompanyStatus, findAllCompanies, findCompany, getCompaniesCbx, removeCompany, saveCompany, updateCompany } from "../controllers/company.controller";
import { checkDuplicateCompany } from "../middleware/verifyCompany";
import { checkDuplicateUsernameOrEmail } from "../middleware/verifySignUp";
import { changeGroupStatus, findAllGroups, findGroupByID, getGroupsCbx, removeGroup, saveGroup, updateGroup } from "../controllers/group.controller";
import { changeSubGroupStatus, findAllSubGroups, findSubGroupByID, getSubgroupsCbx, removeSubGroup, saveSubGroup, updateSubGroup } from "../controllers/subGroup.controller";
import { changeLoadTypeStatus, findAllLoadTypes, findLoadTypeByID, getLoadTypesCbx, removeLoadType, saveLoadType, updateLoadType } from "../controllers/loadType.controller";
import { changeLocationTypeStatus, findAllLocationTypes, findLocationTypeByID, getLocationTypesCbx, removeLocationType, saveLocationType, updateLocationType } from "../controllers/locationType.controller";
import { changeOperationTypeStatus, findAllOperationTypes, findOperationTypeByID, getOperationTypesCbx, removeOperationType, saveOperationType, updateOperationType } from "../controllers/operationType.controller";
import { changeAccessoryStatus, findAccessoryByID, findAllAccessories, removeAccessory, saveAccessory, updateAccessory } from "../controllers/accessory.controller";
import { checkDuplicateGroup, checkDuplicateLoad, checkDuplicateLoadType, checkDuplicateLocationType, checkDuplicateOperationType, checkDuplicatePhotoHold, checkDuplicatePreload, checkDuplicateSubGroup } from "../middleware/verifyDuplicate";
import { changeProcessStatus, changeProcessStatusDescription, findAllProcess, findProcessByID, removeProcess, saveProcess, updateProcess } from "../controllers/process.controller";
import { changeProcessAcessoryStatus, findAllProcessAccessories, findProcessAcessoryByID, findProcessAcessoryByOSID, findProcessAcessoryBySubGroup, removeProcessAcessory, removeAllProcessAcessoryByOsID, saveProcessAcessory, updateProcessAcessory, synCheckList } from "../controllers/process_acessory.controller";
import { changeNomeationStatus, findAllNomeations, findAllSurveyersNomeations, findNomeationByID, removeNomeation, saveNomeation, updateNomeation } from "../controllers/nomeation.controller";
import { changeGroupFileStatus, findAllGroupFiles, findGroupFileByID, removeGroupFile, saveGroupFile, updateGroupFile } from "../controllers/group_file.controller";
import { changeCustomerStatus, findAllCustomers, findCustomerByID, getCustomersCbx, removeCustomer, saveCustomer, updateCustomer } from "../controllers/customer.controller";
import { changeCargoStatus, findAllCargos, findCargoByID, removeCargo, saveCargo, updateCargo } from "../controllers/cargo.controller";
import { changeReportFileStatus, findReportFileByOSID, findReportFileByID, removeReportFile, saveReportFile, updateReportFile } from "../controllers/report_file.controller";
import { changeCargoAccessoryStatus, findAllCargoAccessories, findCargoAccessoryByID, removeCargoAccessory, updateCargoAccessory } from "../controllers/cargo_accessory.controller";
import { findAllShipsFromKMS } from "../controllers/helper.controller";
import { findReportFormByOSID, saveReportForm } from "../controllers/report_form.controller";
import { findReportLoadByOSID, saveReportLoad } from "../controllers/report_load.controller";
import { findReportPreloadByOSID, saveReportPreload } from "../controllers/report_preload.controller";
import { generateReport } from "../controllers/report.controller";
import { findAllHoldInspectionByOSID, findHoldInspectionAndPhotosByKeys, findHoldInspectionByID, findHoldInspectionByKeys, removeHoldInspection, saveHoldInspection, updateHoldInspection } from "../controllers/hold_inspection.controller";
import { findPhotoHoldByID, findPhotoHoldByKeys, removePhotoHold, savePhotoHold, updatePhotoHold } from "../controllers/photo_hold.controller";

export const ApiRoute = (app) => {

  /**
   * REQUEST HEADER
   */

  app.use(function (req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  /**
   * COMBOBOX
   */

  app.post("/api/company/listCbx",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    getCompaniesCbx
  );

  app.post("/api/customer/listCbx",
    [authJwt.verifyToken],
    getCustomersCbx
  );

  app.post("/api/group/listCbx",
    [authJwt.verifyToken],
    getGroupsCbx
  );

  app.post("/api/sub-group/listCbx",
    [authJwt.verifyToken],
    getSubgroupsCbx
  );

  app.post("/api/loadType/listCbx",
    [authJwt.verifyToken],
    getLoadTypesCbx
  );

  app.post("/api/locationType/listCbx",
    [authJwt.verifyToken],
    getLocationTypesCbx
  );

  app.post("/api/operationType/listCbx",
    [authJwt.verifyToken],
    getOperationTypesCbx
  );

  /**
   * CustomUpdate
   */

  app.post("/api/user/updateCustomProfile",
    [authJwt.verifyToken],
    updateUser
  );

  /**
   * LIST (PAGINATED)
   */

  app.post("/api/process/list",
    [authJwt.verifyToken],
    findAllProcess
  );

  app.post("/api/hold_inspection/list",
    [authJwt.verifyToken],
    findAllHoldInspectionByOSID
  );

  app.post("/api/process_acessory/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllProcessAccessories
  );

  app.post("/api/nomeation/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllNomeations
  );

  app.post("/api/group_file/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllGroupFiles
  );

  app.post("/api/customer/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllCustomers
  );

  app.post("/api/cargo/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllCargos
  );

  app.post("/api/report_file/list",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    findReportFileByOSID
  );

  app.post("/api/cargo_accessory/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllCargoAccessories
  );

  app.post("/api/accessory/list",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    findAllAccessories
  );

  app.post("/api/company/list",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    findAllCompanies
  );

  app.post("/api/user/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllUsers
  );

  app.post("/api/group/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllGroups
  );

  app.post("/api/sub-group/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllSubGroups
  );

  app.post("/api/load-type/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllLoadTypes
  );

  app.post("/api/location-type/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllLocationTypes
  );

  app.post("/api/operation-type/list",
    [authJwt.verifyToken, authJwt.isAdmin],
    findAllOperationTypes
  );

  /**
   * LOAD BY KEYS
   */
  
  app.post("/api/hold_inspection/loadByKeys",
    [authJwt.verifyToken],
    findHoldInspectionByKeys
  );

  app.post("/api/hold_inspection_and_photos/loadByKeys",
    [authJwt.verifyToken],
    findHoldInspectionAndPhotosByKeys
  );

  app.post("/api/photo_hold/loadByKeys",
    [authJwt.verifyToken],
    findPhotoHoldByKeys
  );

  /**
   * LOAD
   */

  app.get("/api/process/:id",
    [authJwt.verifyToken],
    findProcessByID
  );

  app.get("/api/hold_inspection/:id",
    [authJwt.verifyToken],
    findHoldInspectionByID
  );

  app.get("/api/photo_hold/:id",
    [authJwt.verifyToken],
    findPhotoHoldByID
  );

  app.get("/api/process_acessory/:id",
    [authJwt.verifyToken],
    findProcessAcessoryByID
  );

  app.get("/api/nomeation/:id",
    [authJwt.verifyToken],
    findNomeationByID
  );

  app.get("/api/group_file/:id",
    [authJwt.verifyToken],
    findGroupFileByID
  );

  app.get("/api/customer/:id",
    [authJwt.verifyToken],
    findCustomerByID
  );

  app.get("/api/cargo/:id",
    [authJwt.verifyToken],
    findCargoByID
  );

  app.get("/api/report_file/:id",
    [authJwt.verifyToken],
    findReportFileByID
  );

  app.get("/api/cargo_accessory/:id",
    [authJwt.verifyToken],
    findCargoAccessoryByID
  );

  app.get("/api/accessory/:id",
    [authJwt.verifyToken],
    findAccessoryByID
  );

  app.get("/api/user/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    findUserByID
  );

  app.get("/api/company/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    findCompany
  );

  app.get("/api/group/:id",
    [authJwt.verifyToken],
    findGroupByID
  );

  app.get("/api/sub-group/:id",
    [authJwt.verifyToken],
    findSubGroupByID
  );

  app.get("/api/load-type/:id",
    [authJwt.verifyToken],
    findLoadTypeByID
  );

  app.get("/api/location-type/:id",
    [authJwt.verifyToken],
    findLocationTypeByID
  );

  app.get("/api/operation-type/:id",
    [authJwt.verifyToken],
    findOperationTypeByID
  );

  /**
   * CREATE
   */

  app.post("/api/process/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveProcess
  );

  app.post("/api/process_accessory/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveProcessAcessory
  );

  app.post("/api/nomeation/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveNomeation
  );

  app.post("/api/group_file/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveGroupFile
  );

  app.post("/api/customer/create",
    [authJwt.verifyToken, authJwt.isAdmin],
    saveCustomer
  );

  app.post("/api/cargo/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveCargo
  );

  app.post("/api/report_file/create",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    saveReportFile
  );

  app.post("/api/company/create",
    [authJwt.verifyToken, authJwt.isSuperAdmin, checkDuplicateCompany],
    saveCompany
  );

  app.post("/api/user/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateUsernameOrEmail],
    saveUser
  );

  app.post("/api/accessory/create",
    [authJwt.verifyToken, authJwt.isAdmin],
    saveAccessory
  );

  app.post("/api/group/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateGroup],
    saveGroup
  );

  app.post("/api/sub-group/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateSubGroup],
    saveSubGroup
  );

  app.post("/api/load-type/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateLoadType],
    saveLoadType
  );

  app.post("/api/location-type/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateLocationType],
    saveLocationType
  );

  app.post("/api/operation-type/create",
    [authJwt.verifyToken, authJwt.isAdmin, checkDuplicateOperationType],
    saveOperationType
  );

  /**
   * UPDATE
   */

  app.post("/api/process/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateProcess
  );

  app.post("/api/process_accessory/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateProcessAcessory
  );

  app.post("/api/nomeation/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateNomeation
  );

  app.post("/api/group_file/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateGroupFile
  );

  app.post("/api/customer/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateCustomer
  );

  app.post("/api/cargo/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateCargo
  );

  app.post("/api/report_file/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateReportFile
  );

  app.post("/api/cargo_accessory/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateCargoAccessory
  );

  app.post("/api/accessory/update",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    updateAccessory
  );

  app.post("/api/company/update",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    updateCompany
  );

  app.post(
    "/api/user/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateUser
  );

  app.post(
    "/api/group/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateGroup
  );

  app.post(
    "/api/sub-group/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateSubGroup
  );

  app.post(
    "/api/load-type/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateLoadType
  );

  app.post(
    "/api/location-type/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateLocationType
  );

  app.post(
    "/api/operation-type/update",
    [authJwt.verifyToken, authJwt.isAdmin],
    updateOperationType
  );

  /**
   * DELETE
   */

  app.delete("/api/process/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeProcess
  );

  app.delete("/api/hold_inspection/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeHoldInspection
  );

  app.delete("/api/photo_hold/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removePhotoHold
  );

  app.delete("/api/process_accessory/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeProcessAcessory
  );

  app.delete("/api/process_accessory/deleteByOSID/:os_id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeAllProcessAcessoryByOsID
  );

  app.delete("/api/nomeation/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeNomeation
  );

  app.delete("/api/group_file/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeGroupFile
  );

  app.delete("/api/customer/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeCustomer
  );

  app.delete("/api/cargo/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeCargo
  );

  app.delete("/api/report_file/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeReportFile
  );

  app.delete("/api/cargo_accessory/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeCargoAccessory
  );

  app.delete("/api/user/delete/:id",
    [authJwt.verifyToken, authJwt.isAdmin],
    removeUser
  );

  app.delete("/api/company/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeCompany
  );

  app.delete("/api/accessory/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeAccessory
  );

  app.delete("/api/group/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeGroup
  );

  app.delete("/api/sub-group/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeSubGroup
  );

  app.delete("/api/load-type/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeLoadType
  );

  app.delete("/api/location-type/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeLocationType
  );

  app.delete("/api/operation-type/delete/:id",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    removeOperationType
  );

  /**
   * Change Situation (Activate or Inactivate)
   */

  app.post("/api/process/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeProcessStatus
  );

  app.post("/api/process_accessory/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeProcessAcessoryStatus
  );

  app.post("/api/nomeation/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeNomeationStatus
  );

  app.post("/api/group_file/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeGroupFileStatus
  );

  app.post("/api/customer/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeCustomerStatus
  );

  app.post("/api/cargo/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeCargoStatus
  );

  app.post("/api/report_file/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeReportFileStatus
  );

  app.post("/api/cargo_accessory/changeSituation",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeCargoAccessoryStatus
  );

  app.post("/api/company/changeSituation",
    [authJwt.verifyToken, authJwt.isSuperAdmin],
    changeCompanyStatus
  );

  app.post(
    "/api/user/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeUserStatus
  );

  app.post(
    "/api/accessory/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeAccessoryStatus
  );

  app.post(
    "/api/group/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeGroupStatus
  );

  app.post(
    "/api/sub-group/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeSubGroupStatus
  );

  app.post(
    "/api/load-type/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeLoadTypeStatus
  );

  app.post(
    "/api/location-type/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeLocationTypeStatus
  );

  app.post(
    "/api/operation-type/changeSituation",
    [authJwt.verifyToken, authJwt.isAdmin],
    changeOperationTypeStatus
  );

  /**
   Change Status
   * */

  app.post("/api/process/changeStatus",
    [authJwt.verifyToken, authJwt.isOperatorOrAdmin],
    changeProcessStatusDescription
  );

  /**
   * Find something from KMS
   */

  app.get("/api/kms/findShip",
    [authJwt.verifyToken],
    findAllShipsFromKMS
  );

  /**
   * API endpoints for App
   */
  
  app.get("/api/app/getUserNomeations",
    [authJwt.verifyToken],
    appGetNomeations
  );

  app.post("/api/app/createReportForm",
    [authJwt.verifyToken],
    saveReportForm
  );

  app.post("/api/app/createReportLoad",
    [authJwt.verifyToken, checkDuplicateLoad],
    saveReportLoad
  );

  app.post("/api/app/createReportPreload",
    [authJwt.verifyToken, checkDuplicatePreload],
    saveReportPreload
  );

  app.post("/api/app/synchecklist",
    [authJwt.verifyToken],
    synCheckList,
  );

  app.post("/api/app/create_hold_inspection",
    [authJwt.verifyToken],
    saveHoldInspection
  );

  app.post("/api/app/create_photo_hold",
    [authJwt.verifyToken, checkDuplicatePhotoHold],
    savePhotoHold
  );

  app.post("/api/app/update_hold_inspection",
    [authJwt.verifyToken],
    updateHoldInspection
  );

  app.post("/api/app/update_photo_hold",
    [authJwt.verifyToken],
    updatePhotoHold
  );

  /**
   * REPORTS app -> Konnectends integration
   */

  app.post("/api/report/loadForm",
    [authJwt.verifyToken],
    findReportFormByOSID
  );

  app.post("/api/report/findLoad",
    [authJwt.verifyToken],
    findReportLoadByOSID
  );

  app.post("/api/report/findPreload",
    [authJwt.verifyToken],
    findReportPreloadByOSID
  );

  app.post("/api/report/findProcessAcessoryBySubGroup",
    [authJwt.verifyToken],
    findProcessAcessoryBySubGroup,
  );

  app.post("/api/report/findProcessAcessoryByOSID",
    [authJwt.verifyToken],
    findProcessAcessoryByOSID
  );

  app.get("/api/report/download/:os_id",
    [authJwt.verifyToken],
    generateReport
  );

  app.post("/api/nomeation_surveyer/list",
    [authJwt.verifyToken, authJwt.isSurveyerOrSuperAdmin],
    findAllSurveyersNomeations
  );

};