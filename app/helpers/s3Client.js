import { S3 } from 'aws-sdk';
import axios from 'axios';
import { readFileSync } from 'fs';
import { findElementByOSIDWithOutResponse, handleResponse } from '../controllers/helper.controller';

const AWS = require('aws-sdk');

const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION, AWS_S3_BUCKET } = process.env;

AWS.config.setPromisesDependency(require('bluebird'));
AWS.config.update({ accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_SECRET_ACCESS_KEY, region: AWS_REGION });

async function uploadFile (fileName, filePath) {
  const s3 = new S3({ apiVersion: '2006-03-01', region: process.env.AWS_REGION });
  const fileContent = readFileSync(filePath);

  const params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Key: fileName,
    Body: fileContent,
  };

  const data = await s3.upload(params).promise();
  return data.Location;
}

async function listObjects (filter) {
  const s3 = new AWS.S3();
  const params = {
    Bucket: process.env.AWS_S3_BUCKET,
    Prefix: decodeURIComponent(filter)
  };

  const result = await s3.listObjectsV2(params).promise();
  return result.Contents.map(item => item.Key);
}

export const getS3File = async (req, res, dbInstance, type) => {
  const name = req.body.name;
  const mainObj = await findElementByOSIDWithOutResponse(req, res, dbInstance, '9ec4d6b0-8026-11ec-b3dd-fbfe4c9432e5', false);
  const s3 = new AWS.S3();
  const params = {
    Bucket: AWS_S3_BUCKET,
    Key: `reports/${name}.${type}`,
  }

  const obj = [];

  mainObj.map((item, idx) => {
    s3.getObject(params, (err, data) => {
      if (err) return Object.assign({ image: null }, item);
      let file = new Buffer.from(data.Body).toString('base64');
      file = "data:" + data.ContentType + ";base64," + file;
      const image = { image: file };
      Object.assign(image, item.dataValues);
      obj.push(item.dataValues);
      // item.dataValues.push(image);
    });
  });

  return handleResponse(res, obj, 201);


  //const fileStream = s3.getObject(params).createReadStream();
  // s3.getObject(params, function (err, data) {
  //   if (err) {
  //     return res.send({ "error": err });
  //   }
  //   let image = new Buffer.from(data.Body).toString('base64');
  //   image = "data:" + data.ContentType + ";base64," + image;
  //   // let response = {
  //   //     "statusCode": 200,
  //   //     "headers": {
  //   //         "Access-Control-Allow-Origin": "*",
  //   //         'Content-Type': data.ContentType
  //   //     },
  //   //     "body":image,
  //   //     "isBase64Encoded": true
  //   // };
  //   console.log("FILE: ", image);
  //   // res.send({ response });
  //   file = image;
  // }).then(e=> {return file});

}

export const downloadS3ImagesToFolder = async (fileList, downloadFolder = `./report/temp_images/`) => {
  const createdImageList = [];
  const resolved = fileList.map(async (item) => {
    try {
      const image = await axios
      .get(item.file, {
          responseType: 'arraybuffer'
      });
      createdImageList.push({...item.dataValues, "path": image.data});
    } catch (error) {
      console.log("Erro ao baixar uma foto do S3", error);
    }
  });
  await Promise.all(resolved);
  return createdImageList;
};

export const fetchImage = async (src) => {
  try {
    const image = await axios.get(src, {
      responseType: 'arraybuffer'
    });
    return image.data;
  } catch (error) {
    console.log("Erro ao baixar uma foto do S3", error);
  }
  
}

export const base64Upload = async (name, base64) => {
  const s3 = new AWS.S3();
  const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');

  let type = base64.split(';')[0].split('/')[1];
  if (type === "9j") type = "JPG";
  const params = {
    Bucket: AWS_S3_BUCKET,
    Key: `reports/${name}.${type}`,
    Body: base64Data,
    ContentType: `image/${type}`,
  };
  let s3Upload = await s3.upload(params).promise();
  console.log("SALVO ARQUIVO NO S3: ", s3Upload);
  s3Upload ? undefined : Logger.error("Error synchronizing the bucket");

  return s3Upload.Location || null;
}

export default { uploadFile, listObjects }



/*
const shipJSON = { 
  SHIP_ID: '460144',
  IMO: '9442873',
  MMSI: '371172000',
  CALLSIGN: '3EQO8',
  SHIPNAME: 'COLUMBIA HIGHWAY',
  TYPE_COLOR: '7',
  LAST_POS: 1654252379,
  CODE2: 'PA',
  COUNTRY: 'Panama',
  COUNT_PHOTOS: '93',
  NEXT_PORT_NAME: 'SANTOS',
  NEXT_PORT_COUNTRY: 'BR',
  NEXT_PORT_ID: '189',
  ETA: 'masked',
  DESTINATION: 'MXVER>BRSSZ',
  CURRENT_PORT_COUNTRY: null,
  TYPE_SUMMARY: 'Cargo',
  COURSE: '84',
  LON: '-60.26152',
  LAT: '11.59533',
  TIMEZONE: '-4',
  ETA_OFFSET: null,
  SPEED: '15.8',
  ETA_UPDATED: '2022-06-03 10:43:00',
  DISTANCE_TO_GO: '3297',
  PORT_ID: null,
  CURRENT_PORT: null,
  CTA_ROUTE_FORECAST: 'true' 
}
*/