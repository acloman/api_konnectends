import { getAccessories, getCheckList, getFormReport, getLoadingPitures, getNomeation, getPreLoadingPitures } from "../../controllers/helper.controller";
import { createWriteStream } from 'fs';
import PDFDocument from 'pdfkit-table';
import * as t from "./constants";
import { downloadS3ImagesToFolder, fetchImage } from "../s3Client";
import { getCustomer, getWOcode } from "../database/os";
import { getFormattedDate, getFormattedDay, getFormattedHour, getFormattedUSDate } from "../dateUtils";
import { getShipByName } from "../database/shipData";

let nomeation = null;
let formReport = null;
let checkList = null;
let imgPreLoadList = null;
let imgLoadList = null;
let os = null;
let customer = null;
let customerName = null;
let shipDetails = null;
let accessories = null;
let damagedVehicles = new Array();
let damagedLoadingVehicles = new Array();

/** TABLES */
const generateTable = async (doc) => {
  let rows = null;
  const { nome_navio, ultimo_porto, proximo_porto } = formReport;
  doc.fontSize(15)
    .text(`${t.VESSEL_MAIN_PARTICULARS}:`, 50, 320, { align: 'left' })
    .moveDown()
    .fontSize(10);
  if (shipDetails && shipDetails.length > 0) {
    const v = shipDetails[0];
    rows = [
      ["Vessel’s name", v.SHIPNAME],
      ["IMO Number", v.IMO],
      ["Call Sign", v.CALLSIGN],
      ["MMSI Number", v.MMSI],
      ["Flag", v.CODE2],
      ["Port of Registry", v.COUNTRY],
      ["Type", v.TYPE_SUMMARY],
      ["Last Port", ultimo_porto],
      ["Next Port", proximo_porto],
      ["Last coordinates", `LAT / LON: { ${v.LAT}, ${v.LON} }, at ${v.ETA_UPDATED} TMZ: ${v.TIMEZONE}`]
    ];
  } else {
    rows = [
      ["Vessel’s name", nome_navio]
    ]  
  }

  const table = {
    headers: [ " ", " " ],
    rows,
  };

  await doc.table(table, { 
    width: 500,
  });
}

const generateCargoDetailsTable = async (doc) => {
  let rows = null;
  const { terminal, berco, fim_operacao, nome_navio } = formReport;
  const { group_name, group_code, load_type, subgroup_name, subgroup_code, locale } = nomeation;
  const dtLoading = getFormattedDay(fim_operacao);
  
  doc.fontSize(15)
    .text(t.DETAIL_CARGO+':', 50, 340, { align: 'left' })
    .moveDown()
    .fontSize(10);
  if (shipDetails && shipDetails.length > 0) {
    const v = shipDetails[0];
    rows = [
      ["Port of Loading", locale],
      // ["Port of Discharge", ""],
      ["Loading Facility", `${berco} ${terminal} - ${locale}`],
      [group_name, group_code],
      ["Model", load_type],
      [`Vehicles (${subgroup_name})`, subgroup_code],
      ["Date of Loading", dtLoading],
      ["Vessel / Destination", `${v.SHIPNAME}`]
    ];
  } else {
    rows = [
      ["Port of Loading", locale],
      // ["Port of Discharge", ""],
      ["Loading Facility", `${terminal} - Port of ${locale}`],
      [group_name, group_code],
      ["Model", load_type],
      [`Vehicles (${subgroup_name})`, subgroup_code],
      ["Date of Loading", dtLoading],
      ["Vessel", nome_navio]
    ]  
  }

  const table = {
    headers: [ " ", " " ],
    rows,
  };

  await doc.table(table, { 
    width: 500,
  });
}

/** END TABLES */

const generateHr = (doc, y) => {
  doc.strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(545, y)
    .stroke();
}

const generateFooter = (doc) => {
  const range = doc.bufferedPageRange();
  doc.fontSize(8)
    .text((range.start + range.count), 0, 780, { align: 'center', width: 100 }); 
}

const generateHeader = (doc) => {
  const {wocode} = os[0];
  const number = t.NOMEATION_NUMBER(wocode);
  const path = './report/logo.jpg';
  
  doc.image(path, 50, 30, { width: 250 })
    .fillColor('#444444')
    .fontSize(5)
    .text(t.K_PARANAGUA, 210, 30, { align: 'right' })
    .text(t.K_PARANAGUA_ADDRESSE, 210, 35, { align: 'right' })
    .text(t.K_PARANAGUA_PHONE, 210, 40, { align: 'right' })
    .text(t.K_PARANAGUA_EMAIL, 210, 45, { align: 'right' })
    .text(t.K_SANTOS, 210, 55, { align: 'right' })
    .text(t.K_SANTOS_ADDRESSE, 210, 60, { align: 'right' })
    .text(t.K_SANTOS_PHONE, 210, 65, { align: 'right' })
    .text(t.K_SANTOS_EMAIL, 210, 70, { align: 'right' })
    .text(t.K_PINHAIS, 210, 80, { align: 'right' })
    .text(t.K_PINHAIS_ADDRESSE, 210, 85, { align: 'right' })
    .text(t.K_PINHAIS_PHONE, 210, 90, { align: 'right' })
    .text(t.K_PINHAIS_EMAIL, 210, 95, { align: 'right' })
    .fillColor('#DA0202')
    .fontSize(10)
    .text(number, 210, 120, { align: 'right' })
    .fillColor('#444444')
    .moveDown();
    generateHr(doc, 110);
}

/** CONTENTS */

/** C1 */
const generateFirstPage = async (doc) => {
  const startSurvey = nomeation.start_survey;
  const { subgroup_code, load_type, locale } = nomeation;
  const cargoType = load_type.toString().toLowerCase();
  const stringArray = subgroup_code.split(/[ ,]+/);
  const { nome_navio, foto_proa, terminal, berco } = formReport;
  const path = await fetchImage(foto_proa);
  const surveyDate = getFormattedDate(startSurvey);
  const today = getFormattedDate();
  const ship = nome_navio.toString().toUpperCase();
  const port = locale.toString().toUpperCase();
  const terminalName = terminal.toString().toUpperCase();

  generateHeader(doc);
  doc.fontSize(10)
    .fillColor('#444444')
    .text(t.REPORT_LOCAL, 50, 120, { align: 'left' })
    .text(t.ISSUED_ON(today), 50, 130, { align: 'left' })
    .fontSize(20)
    .text(t.TITLE, 50, 165, { align: 'center' })
    .fontSize(10)
    .text(t.DESCRIPTION(customerName, surveyDate, stringArray.length, cargoType, nome_navio), 50, 200)
    .image(path, 90, 255, { width: 400, align: 'center' })
    .moveDown()
    .text(t.SHIP_IMG_DETAIL(ship, `${terminalName} ${berco}`, port), 30, 570, { align: 'center' })
    .addPage();
};

/** C2 */
const generateIndexPage = async (doc) => {
  generateHeader(doc);
  doc.fontSize(15)
    .text(t.INDEX+':', 50, 120, { align: 'left' })
    .fontSize(8)
    .text(t.INDEX+';', 50, 150, { align: 'left' })
    .text(t.VESSEL_MAIN_PARTICULARS+';', 50, 163, { align: 'left' })
    .text(t.INTODUTION+';', 50, 176, { align: 'left' })
    .text(t.DETAIL_CARGO+';', 50, 189, { align: 'left' })
    .text(t.OUR_ATTENDANCE+';', 50, 202, { align: 'left' })
    .text(t.CONCLUSION+';', 50, 215, { align: 'left' })
    .text(t.ENCLOSURES+':', 50, 228, { align: 'left' })
    .text(t.PRELOADING_TITLE+';', 70, 241, { align: 'left' })
    .text(t.LOADING_TITLE+';', 70, 251, { align: 'left' })
    .text('', 50, 300, { align: 'left' })
    .moveDown();
    await generateTable(doc);
    doc.addPage();
}

/** C3 */
const generateIntroductionPage = (doc) => {
  const { load_type, subgroup_name, subgroup_code, locale } = nomeation;
  const { nome_navio, inicio_operacao, dt_atracacao, dt_chegada_navio, terminal, berco, agente_navio } = formReport;
  const cargoType = load_type.toString().toLowerCase();
  const stringArray = subgroup_code.split(/[ ,]+/);
  const surveyDate = getFormattedDay(inicio_operacao);
  const dtChegadaPorto = getFormattedDay(dt_chegada_navio);
  const hrChegadaPorto = getFormattedHour(dt_chegada_navio);
  const dtAtracacao = getFormattedDate(dt_atracacao, true);

  generateHeader(doc);
  doc.fontSize(15)
    .text(t.INTODUTION+':', 50, 120, { align: 'left' })
    .fontSize(10)
    .text(t.INTRODUCTION_L1(customerName, surveyDate, stringArray.length, cargoType, nome_navio), 50, 170, { align: 'left' })
    .moveDown()
    .text(t.INTRODUCTION_L2(`${terminal} - ${berco}`, nome_navio, locale, dtChegadaPorto, hrChegadaPorto), { align: 'left' })
    .moveDown()
    .text(t.INTRODUCTION_L3(cargoType, subgroup_name), { align: 'left' })
    .moveDown()
    .text(t.INTRODUCTION_L4(agente_navio, dtAtracacao), { align: 'left' })
    .moveDown()
}

/** C4 */
const generateDetailsOfTheCargoPage = async (doc) => {
  await generateCargoDetailsTable(doc);
  doc.addPage();
}

/** C5 */
const generateOurAttendancePage = (doc) => {
  const { load_type, locale } = nomeation;
  const { dt_chegada_navio, terminal, inicio_operacao, fim_operacao, dt_atracacao } = formReport;
  const dtChegadaPorto = getFormattedDay(dt_chegada_navio);
  const hrChegadaPorto = getFormattedHour(dt_chegada_navio);
  const dtOperation = getFormattedDay(inicio_operacao);
  const hrOperation = getFormattedHour(inicio_operacao);
  const dtEndOperation = getFormattedDay(fim_operacao);
  const hrEndOperation = getFormattedHour(fim_operacao);
  const dtAtracacao = getFormattedDate(dt_atracacao, true);

  const cargoType = load_type.toString().toLowerCase();
  let vehicleAccessories = null;
  accessories.map(item => {
    if ( vehicleAccessories == null)
      vehicleAccessories = item.name;
    else vehicleAccessories = `${vehicleAccessories}, ${item.name}`;
  });

  generateHeader(doc);

  doc.fontSize(15)
    .text(t.OUR_ATTENDANCE+':', 50, 120, { align: 'left' })
    .fontSize(10)
    .text(t.ATTENDANCE_L1(customerName, locale, dtAtracacao, cargoType), 50, 170, { align: 'left' })
    .moveDown()
    .text(t.ATTENDANCE_VEHICLE_L2(cargoType), { align: 'left' })
    .moveDown()
    .text(`( ${vehicleAccessories} )`)
    .moveDown()
    .text(t.ATTENDANCE_VEHICLE_L3(dtChegadaPorto, hrChegadaPorto, dtOperation, hrOperation, terminal), { align: 'left' })
    .moveDown()
    .text(t.ATTENDANCE_VEHICLE_L4(dtEndOperation, hrEndOperation), { align: 'left' })
    .addPage();
}

/** C6 */
const generateNotDamagedConclusionPage = (doc) => {
  const { load_type } = nomeation;
  const cargoType = load_type.toString().toLowerCase();
  const { inicio_operacao } = formReport;
  const surveyDate = getFormattedDay(inicio_operacao);
  let vehicleAccessories = null;
  accessories.map(item => {
    if ( vehicleAccessories == null)
      vehicleAccessories = item.name;
    else vehicleAccessories = `${vehicleAccessories}, ${item.name}`;
  });

  generateHeader(doc);
  doc.fontSize(15)
    .text(t.CONCLUSION+':', 50, 120, { align: 'left' })
    .fontSize(10)
    .text(t.CONCLUSION_L1_NOT_DAMAGED(surveyDate), 50, 170, { align: 'left' })
    .moveDown()
    .text(t.CONCLUSION_VEHICLE_L2(cargoType), { align: 'left' })
    .moveDown()
    .text(`( ${vehicleAccessories} )`)
    .moveDown()
    .text(t.CONCLUSION_VEHICLE_L3(load_type, customerName), { align: 'left' })
    .moveDown()
    .text(t.CONCLUSION_L4, { align: 'left' })
    .moveDown()
    .text(t.CONCLUSION_L5, { align: 'left' })
    .moveDown()
    .text(t.CONCLUSION_L6, { align: 'left' })
    .moveDown()
    .addPage();
}

const generateDamagedConclusionPage = (doc) => {
  const isPreloadDmgd = damagedVehicles.length > 0;
  const isloadDmgd = damagedLoadingVehicles.length > 0;
  const { load_type } = nomeation;
  const cargoType = load_type.toString().toLowerCase();
  const { inicio_operacao } = formReport;
  const surveyDate = getFormattedDay(inicio_operacao);
  let vehicleAccessories = null;
  accessories.map(item => {
    if ( vehicleAccessories == null)
      vehicleAccessories = item.name;
    else vehicleAccessories = `${vehicleAccessories}, ${item.name}`;
  });
  generateHeader(doc);
  if (isPreloadDmgd) {
    let lineR = 220;
    let lineL = 220;
    const colLeft = 52.5;
    const colRight = 302.5;

    doc.fontSize(15)
      .text(t.CONCLUSION+':', 50, 120, { align: 'left' })
      .fontSize(10)
      .text(t.CONCLUSION_L1_DAMAGED(surveyDate), 50, 170, { align: 'left' })
      .moveDown();
      damagedVehicles.map((el, idx) => {
        if (lineR + 200 > (doc.page.height)) {
          lineL = 140;
          lineR = 140;
          doc.addPage();
          generateHeader(doc);
          doc.fontSize(5);
        }
    
        if (idx % 2 == 0) {
          doc.fontSize(5)
            .text(`Evidence of damage, vehicle: ${el.subgroup_id}`, colLeft, (lineL - 5))
            .image(el.path, colLeft, lineL, { width: 242.5, height: 200});
          lineL = lineL + 210;
        } else {
          doc.fontSize(5)
            .text(`Evidence of damage, vehicle: ${el.subgroup_id}`, colRight, (lineR - 5))
            .image(el.path, colRight, lineR, { width: 242.5, height: 200});
          lineR = lineR + 210;
        }
        doc.moveDown(0.5);
      });
    
      doc.addPage();
      generateHeader(doc);
      doc.text("", 50, 150, { align: 'left' })
        .moveDown();
  } else {
    doc.fontSize(15)
      .text(t.CONCLUSION+':', 50, 120, { align: 'left' })
      .fontSize(10)
      .text(t.CONCLUSION_L1_NOT_DAMAGED(surveyDate), 50, 170, { align: 'left' })
      .moveDown();
  }
  
  doc.fontSize(10);
  if (isloadDmgd) {
    doc.text(t.CONCLUSION_DAMAGED_VEHICLE_L2(cargoType), { align: 'left' })
      .moveDown()
      .text(`( ${vehicleAccessories} )`)
      .moveDown()
      .text(t.DAMAGED_CONCLUSION_VEHICLE_L3(load_type, customerName), { align: 'left' })
      .moveDown()
      .text(t.CONCLUSION_L4, { align: 'left' })
      .moveDown()
      .text(t.DAMAGED_CONCLUSION_L5, { align: 'left' })
      .moveDown()
      .text(t.CONCLUSION_L6, { align: 'left' })
      .moveDown()
      .addPage();

    let lineL = 140;
    let lineR = 140;
    const colLeft = 52.5;
    const colRight = 302.5;
      
    generateHeader(doc);
    doc.fontSize(15)
    .text("Evidence of damage(s) (loading)", colLeft, 120);

    damagedLoadingVehicles.map((el, idx) => {
      if (lineR + 200 > (doc.page.height)) {
        lineL = 140;
        lineR = 140;
        doc.addPage();
        generateHeader(doc);
        doc.fontSize(15)
        .text("Evidence of damage(s) (loading)", colLeft, 120);
      }
  
      if (idx % 2 == 0) {
        doc.image(el.path, colLeft, lineL, { width: 242.5, height: 200});
        lineL = lineL + 210;
      } else {
        doc.image(el.path, colRight, lineR, { width: 242.5, height: 200});
        lineR = lineR + 210;
      }
      doc.moveDown(0.5);
    });
    doc.addPage();
    
  } else {
    doc.text(t.CONCLUSION_VEHICLE_L2(cargoType), { align: 'left' })
      .moveDown()
      .text(`( ${vehicleAccessories} )`)
      .moveDown()
      .text(t.CONCLUSION_VEHICLE_L3(load_type, customerName), { align: 'left' })
      .moveDown()
      .text(t.CONCLUSION_L4, { align: 'left' })
      .moveDown()
      .text(t.CONCLUSION_L5, { align: 'left' })
      .moveDown()
      .text(t.CONCLUSION_L6, { align: 'left' })
      .moveDown()
      .addPage();
  } 
  
}

/** C7 - A */
const generatePreLoadInformation = (doc) => {
  let lineR = 140;
  let lineL = 140;
  const colLeft = 52.5;
  const colRight = 302.5;

  generateHeader(doc);
  doc.fontSize(15)
  doc.text(t.ENCLOSURES+': ', 50, 120, { align: 'left' });
  doc.fontSize(10);
  doc.text(t.PRELOADING_TITLE, 190, 123.3, { align: 'left' });

  imgPreLoadList.map((el, idx) => {
    if (lineR + 200 > (doc.page.height)) {
      lineL = 140;
      lineR = 140;
      doc.addPage();
      generateHeader(doc);
    }

    if (idx % 2 == 0) {
      doc.image(el.path, colLeft, lineL, { width: 242.5, height: 200});
      lineL = lineL + 210;
    } else {
      doc.image(el.path, colRight, lineR, { width: 242.5, height: 200});
      lineR = lineR + 210;
    }
    doc.moveDown(0.5);
  });
}

/** C7 - B */
const generateLoadInformation = (doc) => {
  let lineR = 140;
  let lineL = 140;
  const colLeft = 52.5;
  const colRight = 302.5;

  generateHeader(doc);
  doc.fontSize(15);
  doc.text(t.ENCLOSURES+': ', 50, 120, { align: 'left' });
  doc.fontSize(10);
  doc.text(t.LOADING_TITLE, 190, 123.3, { align: 'left' });

  imgLoadList.map((el, idx) => {
    if (lineR + 200 > (doc.page.height)) {
      lineL = 140;
      lineR = 140;
      doc.addPage();
      generateHeader(doc);
    }

    if (idx % 2 == 0) {
      doc.image(el.path, colLeft, lineL, { width: 242.5, height: 200});
      lineL = lineL + 210;
    } else {
      doc.image(el.path, colRight, lineR, { width: 242.5, height: 200});
      lineR = lineR + 210;
    }
    doc.moveDown(0.5);
  });
}

/** END CONTENTS */

const fetchReportData = async (res, os_id) => {
  nomeation = await getNomeation(os_id, res);
  const { subgroup_code } = nomeation;
  const stringArray = subgroup_code.split(/[ ,]+/);
  formReport = await getFormReport(os_id, res);
  const { nome_navio } = formReport;
  shipDetails = await getShipByName(nome_navio);
  checkList = await getCheckList(os_id, res);
  os = await getWOcode(os_id);
  customer = await getCustomer(os[0].cliente_id);
  customerName = customer[0].nome;
  accessories = await getAccessories(os_id, stringArray[0], res)

  const loading = await getLoadingPitures(os_id, res);
  imgLoadList = await downloadS3ImagesToFolder(loading);

  const preLoading = await getPreLoadingPitures(os_id, res);
  imgPreLoadList = await downloadS3ImagesToFolder(preLoading);

  // console.log("DADOS_NAVIO:::\n", shipDetails[0]);
  // console.log("NOMEATION:::", nomeation.dataValues);
  // console.log("FORM:::", formReport.dataValues);
  // console.log("CUSTOMER:::", customer[0]);
  // console.log("OS:::", os);
  // console.log("PRE-LOADS", imgPreLoadList);
}

const generateConclusionPage = (doc) => {
  imgPreLoadList.map(item => {
    if (item.isDamage === 'true' || item.isDamage === true) damagedVehicles.push(item);
  });

  imgLoadList.map(item => {
    if (item.isDamage === 'true' || item.isDamage === true) damagedLoadingVehicles.push(item);
  });

  if (damagedVehicles.length > 0 || damagedLoadingVehicles.length > 0) {
    generateDamagedConclusionPage(doc);
  } else {
    generateNotDamagedConclusionPage(doc);
  }
}

/**
 * Função geral para montagem de relatório de carga rolante.
 * @param {*} res 
 * @param {*} os_id 
 */
export const vehicleCargoReport = async (res, os_id) => {
  /// Recupera os dados necessários para montar o relatório.
  await fetchReportData(res, os_id);
  const { wocode } = os[0];
  const code = wocode.replace('/', '_');
  const reportname = `./report/REPORT_${code}.pdf`;
  /// Instancia o relatório e prepara o header para habilitar e configurar o Content-Disposition.
  let doc = new PDFDocument({ size: 'A4', margin: 50 });
  res.writeHead(200, {
    'Access-Control-Expose-Headers': 'x-access-token, Origin, Content-Type, Accept, Content-Disposition',
    'Content-Type': 'application/pdf',
    'Content-Disposition': `attachment; filename=REPORT_${code}.pdf`,
  });
  /// Monta o relatório (cada função gera uma parte do mesmo).
  doc.pipe(res);
  await generateFirstPage(doc);
  await generateIndexPage(doc);
  generateIntroductionPage(doc);
  await generateDetailsOfTheCargoPage(doc);
  generateOurAttendancePage(doc);
  generateConclusionPage(doc);
  generatePreLoadInformation(doc);
  doc.addPage();
	generateLoadInformation(doc);
	doc.end();
  doc.pipe(createWriteStream(reportname));
}