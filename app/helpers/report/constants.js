
/** Exemplo de saída de dados desejada */
// serviceType -> BREAK BULK CARGO
// surveyDate -> May 05th, 2022
// cargoQtdy -> one / 5
// cargoType -> vehicle
// shipName -> M/V CALIFORNIA HIGHWAY

export const K_NAME = 'Khulmann Surveyors & Consultants';

export const TITLE = 'BREAK BULK CARGO - LOADING REPORT';
export const DESCRIPTION = (companyName, surveyDate, cargoQtdy, cargoType, shipName) => (`THIS IS TO CERTIFY THAT we, Kuhlmann Surveyors & Consultants, on behalf of Messrs. ${companyName}, on ${surveyDate}, we followed the loading of ${cargoQtdy} ${cargoType} into ${shipName}, reporting as follows:`);

export const SHIP_IMG_DETAIL = (nome_navio, bercoTerminal, locale) => (`${nome_navio} BERTHED AT ${bercoTerminal}, ${locale}`);

export const INTRODUCTION_L1 = (companyName, surveyDate, cargoQtdy, cargoType, shipName) => (`At request of ${companyName}, on ${surveyDate}, we followed the loading of ${cargoQtdy} ${cargoType} into ${shipName}.`);
export const INTRODUCTION_L2 = (terminalName, shipName, port, dtChegadaPorto, horaChegadaPorto) => (`The cargo was positioned at ${terminalName}, being expected to be loaded into the ${shipName}, which arrived at ${port} out roads on ${dtChegadaPorto}, at ${horaChegadaPorto} (L.T.).`);
export const INTRODUCTION_L3 = (cargoType, subgroup_name) => (`The purpose of this attendance was to perform a loading survey on the ${cargoType}, (${subgroup_name}, more details in the "4 - Cargo Details" section below) assuring the cargo condition prior to the loading and inspecting the occurrences and the method used on the procedure.`);
export const INTRODUCTION_L4 = (agente_navio, dtAtracacao) => (`In advance, we contacted Messrs. ${agente_navio} in order to know the details of the operation. According to the agent’s representative, the loading was scheduled to start on ${dtAtracacao} (L.T.), so our surveyors were positioned near to the stern to follow the loading and report as follows.`);
export const NOMEATION_NUMBER = (nomeation_name) => (`REPORT KERP ${nomeation_name}`);
export const ATTENDANCE_L1 = (companyName, port, surveyDate, cargoType) => (`As per instructions received from Messrs. ${companyName}, our undersigned surveyors were present at ${port} on ${surveyDate}, in order to inspect the ${cargoType} prior to the loading.`);
export const ATTENDANCE_VEHICLE_L2 = (cargoType) => (`The referred ${cargoType} were previously positioned at TCP’s Patio, were driven alongside the Vessel in 
order to be loaded. Previously to the loading, during the preliminary survey, our representative noticed any 
other damage. The ${cargoType} were found in good conditions and had the following accessories:`);
export const ATTENDANCE_VEHICLE_L3 = (dtChegadaPorto, horaChegadaPorto, dtLoading, hrLoading, deck) => (`The Vessel berthed at ${dtChegadaPorto} (L.T.) on ${horaChegadaPorto} and the loading of the referred vehicles
commenced on ${dtLoading} at ${hrLoading} (L.T.). The vehicles were loaded one by one, driven inside the Vessel
and allocated in ${deck}.`);
export const ATTENDANCE_VEHICLE_L4 = (surveyEndDate, surveyEndHr) => (`We attest that the loading was performed accordingly and was accompanied by our surveyor from the 
beginning to the end. On ${surveyEndDate} at ${surveyEndHr} (L.T.), the cargo was fully loaded, and the operation had 
finished successfully.`);

export const CONCLUSION_TITLE = 'During the loading operation, we could observe de following relevant details:';
export const CONCLUSION_L1_NOT_DAMAGED = ($surveyDate) => (`1) On ${$surveyDate}, during the pre-loading inspection, our representative didn't notice any relevant
damage.`);
export const CONCLUSION_L1_DAMAGED = ($surveyDate) => (`1) On ${$surveyDate}, during the pre-loading inspection, our representative was noticed relevant(s)
damage(s) as folow:`);
export const CONCLUSION_DAMAGED_VEHICLE_L2 = (cargoType) => (`2) The ${cargoType} had the following accessories:`);
export const CONCLUSION_VEHICLE_L2 = (cargoType) => (`2) The ${cargoType} were found in good conditions and had the following accessories:`);
export const CONCLUSION_VEHICLE_L3 = (cargo, companyName) => (`3) The loading of the units occurred in satisfactory conditions, the ${cargo} were duly driven by the drivers 
onto the ${companyName}.`);
export const DAMAGED_CONCLUSION_VEHICLE_L3 = (cargo, companyName) => (`3) During the loading stage, some damage was found in one or more units (Further details on the next page), the ${cargo} were duly driven by the drivers 
onto the ${companyName}.`);
export const CONCLUSION_L4 = 'The cargo was loaded under good weather with help of stevedores.';
export const CONCLUSION_VEHICLE_L5 = `5) The components (stern ramp, steel chains, dans, and others) used in the operation were in acceptable 
physical state to receive the cargo.`;
export const CONCLUSION_L5 = `Based on the above mentioned facts, we concluded that the whole operation was performed under safe,
good and normal circumstances. Further details can be noted in the session enclosures.`;
export const DAMAGED_CONCLUSION_L5 = `Based on the aforementioned facts, we conclude that the entire operation was carried out under safe conditions,
good and normal circumstances, with the exception of damage(s) found. Further details can be noted in the session enclosures.`;
export const CONCLUSION_L6 = 'This report is issued without prejudice and for the benefit of whom it may concern.';

export const REPORT_LOCAL = 'Paranaguá – Paraná/Brazil.';
export const ISSUED_ON = (reportDate) => (`Issued on ${reportDate}.`);
export const INDEX = '1 - INDEX';
export const VESSEL_MAIN_PARTICULARS = '2 – VESSEL’S MAIN PARTICULARS';
export const INTODUTION = '3 – INTRODUCTION';
export const DETAIL_CARGO = '4 – DETAILS OF THE CARGO';
export const OUR_ATTENDANCE = '5 – OUR ATTENDANCE';
export const CONCLUSION = '6 – CONCLUSION';
export const ENCLOSURES = '7 – ENCLOSURES';
export const PRELOADING_TITLE = 'A - Preliminary survey';
export const LOADING_TITLE = 'B - Loading operation';

export const K_PARANAGUA = 'Paranagua';
export const K_PARANAGUA_ADDRESSE = 'Address: Rua Barão do Rio Branco, 942 CEP 83.203-430';
export const K_PARANAGUA_PHONE = 'Phone: +55(41) 2152-7600 Fax:+55(41) 21527633';
export const K_PARANAGUA_EMAIL = 'kuhlmann@kuhlmann.com.br';

export const K_SANTOS = 'Santos';
export const K_SANTOS_ADDRESSE = 'Address: Rua Martin Afonso, 24 sala 31 CEP 11.010-060';
export const K_SANTOS_PHONE = 'Phone: +55(13) 2105-7600 Fax:+55(13) 2105-7633';
export const K_SANTOS_EMAIL = 'kuhlmannsantos@kuhlmann.com.br ';

export const K_PINHAIS = 'Pinhais';
export const K_PINHAIS_ADDRESSE = 'Address: Rua Barão do Rio Branco, 942 CEP 83.203-430';
export const K_PINHAIS_PHONE = 'Phone: +55(41) 99131-5859';
export const K_PINHAIS_EMAIL = 'operations@kuhlmann.com.br';

export const K_SITE = 'http://www.kuhlmann-pandi.com';
