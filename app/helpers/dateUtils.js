import dayjs from "dayjs";

export const getFullYear = () => {
  const today = new Date();
  return today.getFullYear();
};

export const getYear = () => {
  const today = new Date();
  return today.getFullYear().toString().slice(-2);
};

export const getFormattedUSDate = (dtString) => {
  let date = dtString;
  if (date == null || date == '') {
    date = new Date().toLocaleString();
  }
  var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  
  return [year, month, day].join('-');
};

export const getFormattedDate = (date, useAt = false) => {
  let dtString = date;
  if (dtString == null || dtString == '') {
    dtString = new Date().toLocaleString();
  }
  var localizedFormat = require('dayjs/plugin/localizedFormat');
  dayjs.extend(localizedFormat);
  if (useAt) {
    const date = dayjs(dtString).format('LL');
    const hour = dayjs(dtString).format('LT');
    return `${date}, at ${hour}`;
  }
    
  return dayjs(dtString).format('LLLL');
}

export const getFormattedDay = (date) => {
  let dtString = date;
  if (dtString == null || dtString == '') {
    dtString = new Date().toLocaleString();
  }
  var localizedFormat = require('dayjs/plugin/localizedFormat')
  dayjs.extend(localizedFormat)
  return dayjs(dtString).format('LL');
}

export const getFormattedHour = (date) => {
  let dtString = date;
  if (dtString == null || dtString == '') {
    dtString = new Date().toLocaleString();
  }
  var localizedFormat = require('dayjs/plugin/localizedFormat')
  dayjs.extend(localizedFormat)
  return dayjs(dtString).format('LT');
}