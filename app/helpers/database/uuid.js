import { DataTypes } from "sequelize";
import {v4 as uuidv4} from 'uuid';

export const uuidPrimaryKey = () => ({
  allowNull: false,
  type: DataTypes.UUID,
  defaultValue: uuidv4(),
  primaryKey: true,
});
