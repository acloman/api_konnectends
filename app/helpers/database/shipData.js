import axios from 'axios';

export const getShipDetailsByID = async (shipId) => {
  const url = `https://www.marinetraffic.com/en/reports?asset_type=vessels&shipid=${shipId}&vessel-image=000bde43ea7a09dd59a628b914c20cba5803&columns=flag,shipname,photo,recognized_next_port,reported_eta,reported_destination,current_port,imo,ship_type,show_on_live_map,time_of_latest_position,lat_of_latest_position,lon_of_latest_position,notes`
  try {
    const response = await axios.get(url, {
      headers: { 'vessel-image': '000bde43ea7a09dd59a628b914c20cba5803'}
    });
    return response.data.data[0];
  } catch (error) {
    console.log("Erro ao baixar detalhes do navio", error);
  }
}

export const getShipByName = async (shipName) => {
  const url = `https://www.marinetraffic.com/pt/search/searchAsset?what=vessel&term=${shipName}&vessel-image=000bde43ea7a09dd59a628b914c20cba5803`
  try {
    const resp = await axios.get(url, {
      headers: { 'vessel-image': '000bde43ea7a09dd59a628b914c20cba5803'}
    });
    if (resp && resp.data && resp.data.length > 0) {
      const ships = [];
      const resolved = resp.data.map(async(item) => {
        const response = await getShipDetailsByID(item.id);
        ships.push(response);
      });
      await Promise.all(resolved);
      return ships;
    }
  } catch (error) {
    console.log("Erro ao baixar dados do navio", error);
  }
}