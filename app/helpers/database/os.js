/**
 * @author Antonio Carlos Lázaro de Oliveira.
 * @company Entersoft Sistemas e Serviços LTDA.
 */

import { QueryTypes, Sequelize } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { showError } from '../../../server';
import { findShipIDByExactNameFromKMS, newShip } from '../../controllers/helper.controller';
import db from '../../models';
import { getFormattedUSDate, getFullYear, getYear } from '../dateUtils';

const CENTRO_DE_CUSTO_ID = 'd5eadec4-0f34-4a63-b8ba-84521893e5ce';
const FILIAL_ID = '4380f770-b189-11ea-af93-59e5ede4b991';
const PRODUTO_ID = '1c0ef420-2a8b-11ec-b243-5d0e50c19403'
const MOEDA_ID = '27ca0c20-b189-11ea-9d96-8fef984432af';
const STATE_ID = '153d2f40-b184-11ea-95e5-e5cb5068f55d';
const CITY_ID = '555ce320-b185-11ea-bb15-7dca01672f8a';
const STATUS = 'NOMEADA';

export const getOSCode = async () => {
  try {
    const data = await db.sequelize.query(
      'select max(code)+1 as code from base.os', { type: QueryTypes.SELECT }
    );
    return data[0].code;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const generateOSNumber = (code) => {
  const ddd = '041';
  const year = getYear();
  console.log('YEAR', year);
  const codwo = String("000000" + code).slice(-6);
  console.log('WOCODE', `PER${ddd}${codwo}/${year}`);
  return `PER${ddd}${codwo}/${year}`;
};

export const generateShipID = async (transport_type, transport_identification) => {
  if(transport_type === 'Ship') {
    const ship = await findShipIDByExactNameFromKMS(transport_identification);
    if (ship && ship[0] && ship[0].id !== '') return ship[0].id
    else {
      await newShip(transport_identification);
      const ship = await findShipIDByExactNameFromKMS(transport_identification);
      return ship[0].id || '';
    }
  }
  return null;
}

const handleCompanyID = (id) => {
  return id === '7bf47ea1-ada0-4c0d-8104-67f71425cca5' ? '82271020-5757-11eb-aaaf-2302b3ba3228' : id;
}

export const createNewOS = async (user, data) => {
  const { id: userID, name, company_id, email } = user;
  const clienteID = handleCompanyID(company_id);
  const { operation_type, load_type, schedule_date, description, transport_type, transport_identification } = data;
  const code = await getOSCode();
  const wocode = generateOSNumber(code);
  const year = getFullYear();
  const nomeationDate = getFormattedUSDate(new Date());
  const operationDate = getFormattedUSDate(schedule_date);
  const navioID = await generateShipID(transport_type, transport_identification);
  const container = transport_type === 'Container' ? transport_identification : '';
  try {
    let osID = null;
    await db.sequelize.query(
      `INSERT INTO base.os(
         id,
         wocode, 
         year, 
         dt_nomeacao, 
         dt_operacao, 
         status, 
         cliente_id,
         produto_id, 
         handle_name, 
         handle_email, 
         moeda_id, 
         filial_id, 
         centro_custo_id, 
         state_id, 
         city_id, 
         observacao,
         navio_id, 
         tipo_operacao, 
         container, 
         user_id,
         code,
         created_at,
         updated_at
       ) VALUES (
         '${uuidv4()}',
         '${wocode}', 
         '${year}', 
         '${nomeationDate}', 
         '${operationDate}', 
         '${STATUS}', 
         '${clienteID}', 
         '${PRODUTO_ID}', 
         '${name}', 
         '${email}', 
         '${MOEDA_ID}', 
         '${FILIAL_ID}',
         '${CENTRO_DE_CUSTO_ID}',
         '${STATE_ID}',
         '${CITY_ID}',
         '${description}',
         '${navioID}',
         '${operation_type} - ${load_type}',
         '${container}',
         '${userID}',
         '${code}',
         now()::timestamp(0),
         now()::timestamp(0)
       ) RETURNING id`, { type: QueryTypes.INSERT }
    ).then(function (newOS) {
      osID = newOS[0][0].id;
      createOSStatus(osID, userID, nomeationDate);
    });
    return osID;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const createOSStatus = async (osId, userID, nomeationDate) => {
  try {
    await db.sequelize.query(`INSERT INTO base.os_status (
      id,
      os_id,
      user_id,
      status,
      dt_status,
      created_at,
      updated_at
    ) values (
      '${uuidv4()}',
      '${osId}',
      '${userID}',
      '${STATUS}',
      '${nomeationDate}',
      now()::timestamp(0),
      now()::timestamp(0)
    )`, { type: QueryTypes.INSERT })
  } catch (err) {
    showError(err);
    return false;
  }
}

export const getInspectorNomeations = async (id) => {
  try {
    const data = await db.sequelize.query(
      `select distinct on (os.id) 
        os_inspetores.id as os_inspetores_id, 
        os.id as os_id, 
        inspetores.id as inspetores_id, 
        os.wocode, 
        inspetores.nome as inspetor, 
        kne.group_name, 
        kne.group_code, 
        kne.subgroup_name, 
        kne.subgroup_code, 
        kne.load_type,
        kne.operation_type,
        kne.transport_type,
        kne.transport_identification,
        kne.location_type,
        kne.locale,
        kne.name,
        kne.schedule_date,
        kne.particularities,
        navios.nome as navio,
        os.tipo_operacao,
        os.container,
        os.status,
        os_inspetores.quantidade, 
        os_inspetores.servico, 
        produtos.nome as carga,
        dt_nomeacao, 
        os_inspetores.created_at, 
        os_inspetores.updated_at 
      from 
        base.inspetores 
        inner join base.os_inspetores on os_inspetores.inspetor_id = inspetores.id 
        inner join base.os on os.id = os_inspetores.os_id 
        inner join base.os_status on os_status.os_id = os.id 
        left join base.produtos on produtos.id = os.produto_id 
        left join base.navios on navios.id = os.navio_id 
        left join konnectends.k_nomeations as kne on kne.os_id = os.id
      where 
        inspetores.id = '${id}'
        and os.status in ('NOMEADA','OPERANDO')
        and kne.id IS NOT NULL`
        , { type: QueryTypes.SELECT }
    );
    return data;

  } catch (err) {
    showError(err);
    return false;
  }
}

export const getInspectorID = async (email) => {
  try {
    const data = await db.sequelize.query(
      `select inspetor_id
        from base.inspetores_contatos
      where 
        tipo = 'Email'
        and contato = '${email}' LIMIT 1`,
        { type: QueryTypes.SELECT }
    );
    return data;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const getActiveOSListBySurveyer = async (email) => {
  const inspectorIDRow = await getInspectorID(email);
  const inspectorID = inspectorIDRow[0].inspetor_id;
  try {
    const data = await db.sequelize.query(
      `select distinct on (os.id) 
      os_inspetores.id as os_inspetores_id, 
      os.id as os_id, 
      inspetores.id as inspetores_id, 
      os.wocode
    from 
      base.inspetores 
      inner join base.os_inspetores on os_inspetores.inspetor_id = inspetores.id 
      inner join base.os on os.id = os_inspetores.os_id
    where 
      inspetores.id = '${inspectorID}'
      and os.status in ('NOMEADA','OPERANDO')`,
        { type: QueryTypes.SELECT }
    );
    return data;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const getNomeationsSurveyers = async (osList) => {
  try {
    const data = await db.sequelize.query(
      `select *
      from 
        konnectends.k_nomeations as k
      where 
        k.os_id in ('${osList}')`,
        { type: QueryTypes.SELECT }
    );
    return data;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const getWOcode = async (os_id) => {
  try {
    const data = await db.sequelize.query(
      `select * from base.os where id = '${os_id}'`,
        { type: QueryTypes.SELECT }
    );
    return data;
  } catch (err) {
    showError(err);
    return false;
  }
}

export const getCustomer = async (cliente_id) => {
  try {
    const data = await db.sequelize.query(
      `select * from base.clientes where id = '${cliente_id}'`,
        { type: QueryTypes.SELECT }
    );
    return data;
  } catch (err) {
    showError(err);
    return false;
  }
}
